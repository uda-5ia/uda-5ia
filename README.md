# UDA 5IA

UDA della 5IA anno 2020/2021

<h1>Introduzione</h1>
Questa repo servirà a tener traccia dell'avanzamento del lavoro rigurdante l'UDA. <br>
    <h2>Disclaimer</h2>
Ciò che appare in questa repo potrebbe risultare offensivo ad alcune persone, se vi sentite presi in causa in ciò, ce ne scusiamo ma dovete capire che programmare non è sempre una passeggiata.<br>

Potrebbero esserci errrori di grammatica e/o sintassi in alcuni commenti e/o documentazione, adattatevi. Nei commenti non ci faremo problemi ad essere onesti e sinceri con voi, quindi se non scriviamo qualcosa o scriviamo sono stanco ecc. cercatevi a cosa serve.


<h2>Getting started</h2>

<h1>22/01/2021</h1>
Assegnazione parziale dei ruoli e creazione di gruppo del diagramma WBS. <br>

<h1>25/01/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): segue la creazione e l'inserimento delle attività dal 22/01/2021 ad oggi sul diagramma Gantt. Chiusura della milestone WBS. <br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): si occupa della creazione dell'ER, schema logico e codice sql per la creazione delle tabelle <br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): si occupa della progettazione dell'ipertesto secondo la metodologia IFML. <br>
[Canello](https://gitlab.com/_Zaizen_): si occupa della crazione del sito condiviso di Altervista, organizzazione del lavoro, aiuto generico al gruppo e studio delle possibilita di un eventuale css <br>
[Lucietto](https://gitlab.com/denislucietto2): si occupa del php login, della registrazione, delle azioni quando viene eseguito il login con successo e ha iniziato a fare la pagina per l'inserimento di un'evento. <br>

<h1>26/01/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento del gantt e inizio del buisness model canvas.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): sistemazione del codice SQL e integrazione del database in PHP.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): aiuto nella creazione del buisness model canvas.<br>
[Canello](https://gitlab.com/_Zaizen_): continua con la creazione del sito condiviso di Altervista.<br>
[Lucietto](https://gitlab.com/denislucietto2): continua con la pagina per l'inserimento di un evento, sistemazione registrazione, login e azione assieme a [Trevisan](https://gitlab.com/gabrieletrevisan02).<br>
<br>

<h1>04/02/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento del gantt e analisi dei rischi.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): discussione sull'eventuale modifica del database per l'integrazione delle immagini.<br>
[Lucietto](https://gitlab.com/denislucietto2): discussione sull'eventuale modifica del database per l'integrazione delle immagini.<br>
[Canello](https://gitlab.com/_Zaizen_): ricerca per la stilizzazione del sito.<br>
<br>

<h1>05/02/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento del gantt, del Business Model Canvas e dell'analisi dei rischi.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): aiuto nella creazione del Business Model Canvas e dell'analisi dei rischi. <br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): si occupa di bug fix e discute sull'inserimento di immagini nei post.<br>
[Lucietto](https://gitlab.com/denislucietto2): si occupa di bug fix e discute sull'inserimento di immagini nei post.<br>
[Canello](https://gitlab.com/_Zaizen_): continua con la ricerca per la stilizzazione del sito.<br>
<br>

<h1>10/02/2021</h1>
[Trevisan](https://gitlab.com/gabrieletrevisan02): si occupa di bug fix.<br>

<h1>11/02/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): supporto nell'aggiornamento della documentazione e nella stilizzazione del sito. <br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): aggiunta categorie e bug-fix.<br>
[Lucietto](https://gitlab.com/denislucietto2): aiuto a [Trevisan](https://gitlab.com/gabrieletrevisan02) ad aggiungere le categorie, e a trovarne di nuove, discussione riguardante bugfix e implementazione php-react. Aiuto a [Canello](https://gitlab.com/_Zaizen_) per la UI del sito react.<br>
[Canello](https://gitlab.com/_Zaizen_): stilizzazione del sito e manutenzione repo.<br>
<br>

<h1>18/02/2021</h1>
[Trevisan](https://gitlab.com/gabrieletrevisan02): aggiunta categorie, feature implementation e bug-fix.<br>
[Lucietto](https://gitlab.com/denislucietto2): aiuto ad aggiungere le categorie, in feature implementation e in bug-fix. <br>
[Canello](https://gitlab.com/_Zaizen_): stilizzazione del sito e manutenzione repo.<br>
<br>

<h1>19/02/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione, supporto nella scelta e nella sistemaione della palette colori.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): supporto nella stilizzazione del sito, nella scelta e nella sistemazione della palette colori. <br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): aggiunta categorie, feature implementation e bug-fix.<br>
[Lucietto](https://gitlab.com/denislucietto2): aggiunta categorie, feature implementation e bug-fix.<br>
[Canello](https://gitlab.com/_Zaizen_): stilizzazione del sito e manutenzione repo.<br>
<br>

<h1>25/02/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione, gestione del team meeting su codice php fatto e da implementare nelle future sessioni.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): team meeting su codice php fatto e da implementare nelle future sessioni.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): completamento visualizzazione eventi, feature implementation e bug-fix.<br>
[Lucietto](https://gitlab.com/denislucietto2): creazione file php e javascript per la visualizzazione degli eventi del database, completamento dei file per l'inserimento degli eventi nel database, feature implementation e bug-fix.<br>
[Canello](https://gitlab.com/_Zaizen_): stilizzazione del sito e manutenzione repo.<br>
<br>

<h1>26/02/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione, gestione del team meeting su codice php fatto e da implementare nelle future sessioni.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): team meeting su codice php fatto e da implementare nelle future sessioni.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): completamento visualizzazione eventi, feature implementation e bug-fix.<br>
[Lucietto](https://gitlab.com/denislucietto2): creazione file php e javascript per la visualizzazione degli eventi del database, completamento dei file per l'inserimento degli eventi nel database, feature implementation e bug-fix.<br>
[Canello](https://gitlab.com/_Zaizen_): stilizzazione del sito e manutenzione repo.<br>
<br>

<h1>04/03/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione e mockup.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): mockup.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): backend.<br>
[Lucietto](https://gitlab.com/denislucietto2): creazione route (backend).<br>
[Canello](https://gitlab.com/_Zaizen_): css e varie.<br>
<br>

<h1>05/03/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione, mockup e CPM.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): mockup e ipertesto.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): backend.<br>
[Lucietto](https://gitlab.com/denislucietto2): creazione contesto condiviso (bakcend).<br>
[Canello](https://gitlab.com/_Zaizen_): css e varie.<br>
<br>

<h1>11/03/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione e aiuto nel testing.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): mockup e ipertesto.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): backend.<br>
[Lucietto](https://gitlab.com/denislucietto2): eliminazione tema e implementazione nel contesto (backend).<br>
[Canello](https://gitlab.com/_Zaizen_): css e varie.<br>
<br>

<h1>12/03/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione e aiuto nel testing.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): mockup e ipertesto.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): backend.<br>
[Lucietto](https://gitlab.com/denislucietto2): eliminazione toggles e reimplementazione in modo più efficente (bakcend).<br>
[Canello](https://gitlab.com/_Zaizen_): css e varie.<br>
<br>

<h1>18/03/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione e aiuto nel testing.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): testing.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): backend.<br>
[Lucietto](https://gitlab.com/denislucietto2): logica funzionale di Login (bakcend).<br>
[Canello](https://gitlab.com/_Zaizen_): css e varie.<br>
<br>

<h1>19/03/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione, setup VM a casa e aiuto nel testing.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): testing e setup VM a casa.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): backend.<br>
[Lucietto](https://gitlab.com/denislucietto2): logica funzionale di Navbar (backend).<br>
[Canello](https://gitlab.com/_Zaizen_): css e varie.<br>
<br>

<h1>25/03/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione e ricerca su frontend React.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): ricerca su frontend React.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): lavoro sul backend React.<br>
[Lucietto](https://gitlab.com/denislucietto2): logica funzionale di Registrazione (backend).<br>
[Canello](https://gitlab.com/_Zaizen_): css, fontend React e varie.<br>
<br>

<h1>08/04/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): setup VM a scuola, aggiornamento documentazione e sistemazione tema (registrazione) e ricerca su frontend React.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): setup VM a scuola, sistemazione tema (registrazione) e ricerca su frontend React.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): registrazione (backend).<br>
[Lucietto](https://gitlab.com/denislucietto2): setup VM a suola, aggiornamento documentazione e sistemazione tema (registrazione e login) e ricerca su frontend React, implementazione remember me.<br>
[Canello](https://gitlab.com/_Zaizen_): setup VM a suola, frontend React(inserisci eventi).<br>
<br>

<h1>09/04/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): aggiornamento documentazione e sistemazione tema (inseriscievento, footer e navbar).<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): rework api PHP per React.<br>
[Lucietto](https://gitlab.com/denislucietto2): costanti context registrazione.<br>
[Canello](https://gitlab.com/_Zaizen_): frontend/backend React(footer) e debug.<br>
<br>

<h1>13/04/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): sistemazione temi e testing/bugfix.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): backend.<br>
[Lucietto](https://gitlab.com/denislucietto2): inserisci evento, utente e bugfix.<br>
[Canello](https://gitlab.com/_Zaizen_): frontend React(footer) e bugfix.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): testing e bugfix.<br>
<br>

<h1>14/04/2021</h1>
[Trevisan](https://gitlab.com/gabrieletrevisan02): backend.<br>
[Lucietto](https://gitlab.com/denislucietto2): inserisci evento e bugfix.<br>
[Canello](https://gitlab.com/_Zaizen_): frontend React e bugfix.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): apprendimento e bugfix.<br>
<br>

<h1>16/04/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): sistemazione documentazione.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): carica commenti (backend).<br>
[Lucietto](https://gitlab.com/denislucietto2): frontend evento.<br>
[Canello](https://gitlab.com/_Zaizen_): frontend evento.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): sistemazione ipertesto.<br>
<br>

<h1>20/04/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): sistemazione frontend.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): bugfix.<br>
[Lucietto](https://gitlab.com/denislucietto2): bugfix.<br>
[Canello](https://gitlab.com/_Zaizen_): frontend visualizza evento e bugfix.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): bugfix.<br>
<br>

<h1>21/04/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): sistemazione frontend.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): bugfix.<br>
[Lucietto](https://gitlab.com/denislucietto2): bugfix.<br>
[Canello](https://gitlab.com/_Zaizen_): frontend visualizza evento e bugfix.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): bugfix.<br>
<br>

<h1>22/04/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): sistemazione frontend.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): bugfix e finalizzazione.<br>
[Lucietto](https://gitlab.com/denislucietto2): bugfix e finalizzazione.<br>
[Canello](https://gitlab.com/_Zaizen_): frontend visualizza evento e bugfix.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): bugfix e finalizzazione.<br>
<br>

<h1>23/04/2021</h1>
[Baldo](https://gitlab.com/Lorenzobaldo02): chiusura del progetto e consegne varie.<br>
[Trevisan](https://gitlab.com/gabrieletrevisan02): chiusura del progetto.<br>
[Lucietto](https://gitlab.com/denislucietto2): chiusura del progetto.<br>
[Canello](https://gitlab.com/_Zaizen_): chiusura della Repo.<br>
[Rizzolo](https://gitlab.com/FilippoRizzolo): chiusura del progetto.<br>
<br>

Zaizen <a href="https://liberapay.com/_Zaizen_/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support Zaizen"/></a>  
Denny<a href="https://liberapay.com/Dannyer/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

<br><br><br><div><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</div>
