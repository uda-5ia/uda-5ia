create table post(
	ID int auto_increment not null,
	commento longtext not null,
	voto enum("1", "2", "3", "4", "5") not null default "3",
	permessoMinimo enum("A", "S") not null default "A",
	nicknameUtente varchar(32) not null,
	IDEvento int not null,
	primary key(ID),
	foreign key(nicknameUtente) references membro(nickname),
	foreign key(IDEvento) references evento(ID)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;


/*a. elenco degli eventi già svolti, in ordine alfabetico di provincia*/
select evento.luogo, evento.dataEvento, evento.titolo
from evento
where evento.dataEvento > CURDATE()
order by evento.luogo ASC;

/*b. elenco dei membri che non hanno mai inserito un commento*/
select membro.nickname, membro.come, membro.cognome, membro.email, membro.provincia
from membro, post
where membro.nickname not in post.autore;

/*c. per ogni evento il voto medio ottenuto in ordine di categoria e titolo*/
select T.votoMedio, evento.categoria, evento.titolo
from evento
inner join (
    select post.eventoRecensito, avg(post.voto) as 'votoMedio'
    from post
    group by post.eventoRecensito
) T on (evento.id = T.eventoRecensito)
order by evento.categoria, evento.titolo;

/*d. i dati dell'utente che ha registrato il maggior numero di eventi*/
SELECT Membro.Nickname, Membro.Nome, Membro.Cognome, Membro.Email, Membro.ProvinciaDiResidenza 
FROM (
    SELECT NicknameMembro, COUNT(NicknameMembro) AS 'NickCount' 
    FROM Evento 
    GROUP BY NicknameMembro 
    ORDER BY NickCount DESC 
    LIMIT 1
) AS E 
INNER JOIN(Membro) 
ON (Membro.Nickname = E.NicknameMembro)