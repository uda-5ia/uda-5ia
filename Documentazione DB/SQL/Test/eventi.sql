insert into artista values
("Nome arte 1", "Nome", "Cognome"),
("Nome arte 2", "Nome2", "Cognome2"),
("Nome arte 3", "Nome3", "Cognome3");

insert into membro values
("Membro1", "Membro", "Cognome", "email@example.com", "S", "passwordCriptata", "Padova"),
("Membro2", "Membro2", "Cognome2", "email2@example.com", "S", "passwordCriptata2", "Milano"),
("Membro3", "Membro3", "Cognome3", "email3@example.com", "S", "passwordCriptata3", "Bologna");

insert into interesse values
("Membro1", "Jazz"),
("Membro1", "Festival"),
("Membro2", "Jazz"),
("Membro2", "Prosa"),
("Membro3", "Visite guidate"),
("Membro3", "Calcio");

insert into evento(dataEvento, titolo, descrizione, permessoMinimo, provinciaEvento, nicknameMembro) values
("2021-05-08 15:00:00", "Evento1", "Descrizione evento1", "A", "Padova", "Membro1"),
("2021-05-18 15:00:00", "Evento2", "Descrizione evento2", "A", "Rovigo", "Membro1"),
("2021-11-04 16:00:00", "Evento3", "Descrizione evento3", "A", "Vicenza", "Membro1"),
("2021-05-18 18:00:00", "Evento4", "Descrizione evento4", "A", "Como", "Membro2"),
("2021-11-04 11:00:00", "Evento5", "Descrizione evento5", "A", "MIlano", "Membro2");

/*Potresti dover modifiare gli id a mano*/
insert into partecipazioneArtista values
(1, "Nome arte 1"),
(1, "Nome arte 2"),

(2, "Nome arte 3"),

(3, "Nome arte 2"),
(3, "Nome arte 3"),

(4, "Nome arte 2"),

(5, "Nome arte 1");

/*Potresti dover modifiare gli id a mano*/
insert into categoriaEvento values
(1, "Festival"),
(2, "Jazz"),
(3, "Visite guidate"),
(4, "Calcio"),
(5, "Prosa");

/*Potresti dover modifiare gli id a mano*/
insert into post(commento, voto, permessoMinimo, nicknameUtente, IDEvento) values
("Commento per un evento", "3", "A", "Membro1", 1),
("Commento per un altro evento", "2", "A", "Membro3", 2),
("Commento per un altro evento ancora", "5", "A", "Membro3", 4);
