/*
Provincia(Provincia, Regione)
Membro(Nickname, Nome, Cognome, Email, Tipo, Password, ProvinciaDiResidenza)
Categoria(Nome)
Interesse(NicknameMembro, NomeCategoria)
Evento(ID, Data, Titolo, Descrizione, PermessoMinimo, ProvinciaEvento, NicknameMembro)
CategoriaEvento(IDEvento, NomeCategoria)
Artista(NomeArte, Nom, Cognome)
PartecipazioneArtista(IdEvento, NomeArte)
Post(ID, Commento, Voto, PermessoMinimo, NicknameUtente, IDEvento)
*/

create table provincePerRegione(
	provincia varchar(25) not null,
	regione varchar(25) not null,
	primary key(provincia)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

create table membro(
	nickname varchar(32) not null,
	nome varchar(32) not null,
	cognome varchar(32) not null,
	email varchar(320) not null,
	/*
	Non sono matto, la scelta della lunghezza è stata fatta in base a questa risposta
	https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address#:~:text=%22There%20is%20a%20length%20limit,total%20length%20of%20320%20characters.
	*/
	tipo enum("A", "M", "S") not null default "S",
	password varchar(255) not null,
	provinciaDiResidenza varchar(25), /*Può essere null*/
	primary key(nickname),
	foreign key(provinciaDiResidenza) references provincePerRegione(provincia)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

create table categoria(
	nome varchar(64) not null,
	tipologia varchar(32) not null default "Altro",
	primary key(Nome)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

create table interesse(
	nicknameMembro varchar(32) not null,
	nomeCategoria varchar(64) not null,
	primary key(nicknameMembro, nomeCategoria),
	foreign key(nicknameMembro) references membro(nickname),
	foreign key(nomeCategoria) references categoria(nome)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

create table evento(
	ID int auto_increment not null,
	dataEvento datetime not null,
	titolo varchar(64) not null,
	descrizione longtext not null,
	permessoMinimo enum("A", "M", "S") not null default "A",
	provinciaEvento varchar(25) not null,
	nicknameMembro varchar(32) not null,
	primary key(ID),
	foreign key(provinciaEvento) references provincePerRegione(provincia),
	foreign key(nicknameMembro) references membro(nickname)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

create table categoriaEvento(
	IDEvento int not null,
	nomeCategoria varchar(64) not null,
	primary key(IDEvento, nomeCategoria),
	foreign key(IDEvento) references evento(ID),
	foreign key(nomeCategoria) references categoria(nome)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

create table artista(
	nomeArte varchar(32) not null,
	nome varchar(32) not null,
	cognome varchar(32) not null,
	primary key(nomeArte)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

create table partecipazioneArtista(
	IDEvento int not null,
	nomeArte varchar(32) not null,
	primary key(IDEvento, nomeArte),
	foreign key(IDEvento) references evento(ID),
	foreign key(nomeArte) references artista(nomeArte)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;

create table post(
	ID int auto_increment not null,
	dataCommento datetime not null,
	commento longtext not null,
	voto enum("1", "2", "3", "4", "5") not null default "3",
	permessoMinimo enum("A", "M", "S") not null default "M",
	nicknameUtente varchar(32) not null,
	IDEvento int not null,
	primary key(ID),
	foreign key(nicknameUtente) references membro(nickname),
	foreign key(IDEvento) references evento(ID)
) Engine = InnoDB default charset = utf8mb4 collate = utf8mb4_unicode_ci;
