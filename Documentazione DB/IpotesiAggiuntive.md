Ipotesi aggiuntive:
1. Post<br/>
    1.1. Ogni commento ha un permesso minimo richiesto per la sua modifica
2. Membro<br/>
    2.1. Ogni membro ha un campo che descrive la tipologia di permesso<br/>
    2.2. Ogni membro può NON inserire la provincia di residenza
3. provincePerRegione<br/>
    3.1. Ogni provincia è univoca nel territorio nazionale
4. Categoria<br/>
    4.1. Ogni categoria ha una tipologia la quale identifica il genere della categoria
5. Artista<br/>
    5.1. Ogni artista ha un nome d'arte univoco
6. Evento<br/>
    6.1. Ogni evento ha un permesso minimo richiesto per la sua modifica<br/>
    6.2. È necessario un artista per ogni evento
