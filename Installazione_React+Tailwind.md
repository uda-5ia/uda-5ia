<h1>Guida all'uso di Tailwindcss + React </h1>
<h2>Queste istruzioni sono valide solo per debian/ubuntu o sistemi affini </h1>
<p>
Se e' la prima volta che provi a creare un server React allora per prima cosa devi eseguire questo comando, esso clonerà la repo, installerà il necessario ed avvierà il server

`git clone https://<USERNAME>:<PASSWORD>@gitlab.com/uda-5ia/uda-5ia.git; cd uda-5ia/Pagine_sito_web/react_php; curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash; source ~/.profile; nvm install 15.8.0; nvm use 15.8.0; npm install --global yarn; yarn add react-transition-group tailwindcss@npm:@tailwindcss/postcss7-compat @tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9 @craco/craco axios react-router-dom react-datepicker react-select ;`

Se devi aggiornare il sito:
All'interno della repo esegui:

`git fetch && git pull`

Se vuoi avviare il server:

`yarn start`

<h1>In caso non funzionassero i comandi sopra usa questi:</h1>
<h2>Installazione essenziali:</h2>

`cd ~/;sudo apt update; curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash;export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")";[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"; nvm install node; nvm use node; nvm install-latest-npm; npm install --global yarn; npm install -g create-react-app;`

<h2>Setup server per la prima volta:</h2>

`cd ~/; sudo rm -rf ~/uda-5ia/; sudo rm -rf ~/react_php/; git clone https://<USERNAME>:<PASSWORD>@gitlab.com/uda-5ia/uda-5ia.git; create-react-app react_php; cp -rf ~/uda-5ia/Pagine_sito_web/react_php/ ~/; cd ~/react_php/; yarn add react-transition-group tailwindcss@npm:@tailwindcss/postcss7-compat @tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9 @craco/craco axios react-router-dom react-datepicker react-select ;`

<h2>Server start:</h2>

`cd ~/react_php/; yarn start;`

<h2>Aggiornare il server e farlo partire:</h2>

`cd ~/uda-5ia/; git fetch; git pull; cd ~/; sudo rm -rf ~/react_php/; create-react-app react_php; cp -rf ~/uda-5ia/Pagine_sito_web/react_php/ ~/; cd ~/react_php/; yarn add react-transition-group tailwindcss@npm:@tailwindcss/postcss7-compat @tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9 @craco/craco axios react-router-dom react-datepicker react-select ; yarn start;`

Il server e' accessibile sulla porta 3000.

<h2>Uso con VMWare</h2>
Se per creare il server stai usando VMWare allora puoi usare anche i seguenti comandi per la condivisone e testing.

1. Abilita la cartella condivisa dalle impostazione della VM.
2. Seleziona il percorso e dai come nome alla cartella "UDA".
3. Usa il seguente comando (ATTENZIONE: questo comando elimina la cartella ~/react-js/ quindi assicurati di fare un backup in caso tu ci stessi lavorando) `cd ~/; sudo rm -rf ~/react_php/; create-react-app react_php; cp -rf /mnt/hgfs/UDA/Pagine_sito_web/react_php/ ~/; cd ~/react_php/;yarn add react-transition-group tailwindcss@npm:@tailwindcss/postcss7-compat @tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9 @craco/craco axios react-router-dom react-datepicker react-select ; yarn start;`
4. Adesso il server che e' appena partito si basa sulla tua versione locale del pc "host". Ogni volta che modifichi i file locali ricrea il server.
