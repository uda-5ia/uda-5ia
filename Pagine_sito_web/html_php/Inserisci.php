<?php

/*

TODO:
2. Controllare in base all'SQL

Da testare:

inserimento piu artisti
inserimento categorie

*/


session_start();
require_once("SendToLogin.php");
require_once("ConnessioneDB.php");
?>
<html>
<head>

  <title>Inserimento evento Community web</title>
  <link rel="stylesheet" href="./css/Inserisci.css">

</head>

<body>

  <form method="post" action="" name="insert-event-form">
    <div class = "center">
      <div class="input">
        <label>Titolo*</label>
        <input type="text" name="titolo" maxlength="64" required />
      </div>
      <br>
      <div class = "inputCategoria">
        <h3>Categorie*</h3>
        <?php

        $sql = $connessione->prepare("SELECT * FROM categoria ORDER BY tipologia, nome ASC");
        $result = $sql->execute();
        $tip_cat = [];
        if($result === TRUE){

          $result = $sql->get_result();

          while($row = $result->fetch_array()){
            $cat = $row[0];
            $tipologia = $row[1];
            if(!isset($tip_cat[$tipologia])){
              $tip_cat[$tipologia] = [];
            }
            $tip_cat[$tipologia][] = $cat;
          }

          $id = 0;
          foreach ($tip_cat as $tipologia => $cats) {
            echo "<h4>$tipologia</h4>";
            foreach ($cats as $cat) {
              echo "<div class = \"input\"><label for = '$id'>$cat</label><input type=\"checkbox\" id = '$id' name=\"categorie[]\" value=\"".$cat."\"/></div>";
              $id++;
            }
          }

        } else {
          echo "<p>Qualcosa e' andato storto!</p>";
        }

        ?>
      </div>
      <br>
      <div class="input">
        <label for = "regione_select">Regione*</label>
        <select id = "regione_select" required>
          <option value = ""></option>
          <?php
          $sql = $connessione->prepare("SELECT DISTINCT regione FROM provincePerRegione ORDER BY regione ASC");
          $result = $sql->execute();
          if($result === TRUE){

            $result = $sql->get_result();

            while($row = $result->fetch_array()){

              echo "<option value = '" . urlencode($row[0]) . "'>" . htmlspecialchars($row[0]) . "</option>";

            }

          } else {
            echo "<p>Qualcosa e' andato stornto!</p>";
          }
          ?>
        </select>
        <div id = "div_provincia" style = "display: none;">
          <label for="provincia_select">Provincia*</label>
          <select id = "provincia_select" name = "provincia" required>
          </select>
        </div>
      </div>
      <br>
      <div class="input">
        <label>Data*</label>
        <input type="date" name="data" required />
      </div>
      <br>
      <div class = "input">
        <label>Artista*</label>
        <?php

        $sql = $connessione->prepare("SELECT * FROM artista ORDER BY nomeArte, nome, cognome ASC");
        $result = $sql->execute();
        if($result === TRUE){

          $result = $sql->get_result();

          $id=0;
          while($row = $result->fetch_array()){
            echo "<div class = \"input\"><label for = '$id'>".$row[0]."(".$row[1].", ".$row[2].")</label><input type=\"checkbox\" id = '$id' name=\"artisti[]\" value=\"".$row[0]."\"/></div>";
          }

        } else {
          echo "<p>Qualcosa e' andato stornto!</p>";
        }

        ?>
      </div>
      <br>
      <div class = "input">
        <label>Descrizione*</label>
        <input type="text" name="descrizione" maxlength="1073741999" required />
      </div>
      <button type="submit" name="invia" value="invia">Invia</button>
    </div>
  </form>
  <a href="./Azione.php">Indietro</a>

  <?php

  if (isset($_POST['invia']) && isset($_POST['titolo']) && isset($_POST['provincia']) && isset($_POST['data']) && isset($_POST['artisti']) && isset($_POST['descrizione']) && isset($_POST['categorie'])) {
    $titolo = htmlentities(substr($_POST['titolo'], 0, 64));
    $provincia = htmlentities(substr($_POST['provincia'], 0, 25));
    $data = htmlentities(substr($_POST['data'], 0, 11));
    $artisti = $_POST['artisti'];
    $descrizione = htmlentities(substr($_POST['descrizione'], 0, 1073742000));
    $categorie = $_POST['categorie'];
    $id;

    $sql = $connessione->prepare("SELECT * FROM evento WHERE titolo = ? AND dataEvento = ?");// controllare compatibilita data DB e data inserita
    $sql->bind_param("ss", $titolo, $data);
    $result = $sql->execute();

    if ($result === TRUE) {

      $result = $sql->get_result()->fetch_assoc();

      if ($result >= 1) {

        echo "Evento precedentemente inserito.";

      }else{

        $sql = $connessione->prepare("INSERT INTO evento (dataEvento, titolo, descrizione, provinciaEvento, nicknameMembro) VALUES (?, ?, ?, ?, ?)");// controllare compatibilita data DB e data inserita
        $sql->bind_param("sssss", $data, $titolo, $descrizione, $provincia, $_SESSION['user_nickname']);
        $result = $sql->execute();
        $id = $connessione->insert_id;

        if ($result === TRUE) {

          $sql = $connessione->prepare("INSERT INTO categoriaEvento (IDEvento, nomeCategoria) VALUES (?, ?)");
          $sql->bind_param("is", $id, $categoria);

          foreach($categorie as $categoria){
            $categoria = htmlspecialchars(substr($categoria, 0, 64));
            //echo "$categoria ".substr($categoria, 0, 64)."<br/>";
            $result = $sql->execute();
            if($result === FALSE){
              break;
            }
          }

          if ($result === TRUE) {
            $sql = $connessione->prepare("INSERT INTO partecipazioneArtista (IDEvento, nomeArte) VALUES (?, ?)");
            $sql->bind_param("is", $id, $artista);
            foreach($artisti as $artista){
              $artista = htmlspecialchars(substr($artista, 0, 32));
              $result = $sql->execute();
              if($result === FALSE){
                break;
              }
            }

            if ($result === TRUE) {

              echo "Tutto perfetto";

            }else{

              echo "erore inserimento aritsta.";
              /*
              da cancellare la registrazione in caso di falliemnto
              */
            }

          }else{

            echo "erore inserimento categoria.";
            /*
            da cancellare la registrazione in caso di falliemnto
            */
          }



        }else{

          echo "erore inserimento evento.";

        }

      }

    }else{

      echo "erore evento precedentemente inserito.";

    }
  }

  $connessione->close();
  ?>
  <script type="text/javascript" src="./js/Province.js"></script>
</body>

</html>
