<?php
/*
TODO
vedi commento sotto
*/

session_start();

$PAGE_LEN = 5;

$regione    = isset($_GET["regione"])    ? $_GET["regione"]    : "";
$provincia  = isset($_GET["provincia"])  ? $_GET["provincia"]  : "";
$categorie  = isset($_GET["categorie"])  ? $_GET["categorie"]  : [];
$artisti    = isset($_GET["artisti"])    ? $_GET["artisti"]    : [];
$dataInizio = isset($_GET["dataInizio"]) ? $_GET["dataInizio"] : "";
$dataFine   = isset($_GET["dataFine"])   ? $_GET["dataFine"]   : "";
$pagina 	= isset($_GET["pagina"])   	 ? $_GET["pagina"]     : "";
$ricerca 	= isset($_GET["ricerca"])    ? $_GET["ricerca"]    : "";

require_once("ConnessioneDB.php");

$categorie_join = false;
$artisti_join = false;

$params_to_bind = "";
$params_to_bind_names = [];

$sql_regione = "";
$sql_provincia = "";
$sql_categorie = "";
$sql_artisti = "";
$sql_data = "";
$sql_ricerca = "";

$offset = 0;

if(strlen($regione) > 0 && strlen($provincia) <= 0){
	$sql_regione = "evento.provinciaEvento IN (" .
	"SELECT provincia
	FROM provincePerRegione
	WHERE regione = ?
)";
$params_to_bind .= "s";
$params_to_bind_names[] = &$regione;
}elseif(strlen($provincia) > 0){
	$sql_provincia = "evento.provinciaEvento = ?";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$provincia;
}

foreach ($categorie as $categoria) {
	$sql_categorie .= "categoriaEvento.nomeCategoria = ? OR ";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$categorie[array_search($categoria, $categorie)];
	$categorie_join = true;
}
$sql_categorie = substr($sql_categorie, 0, -4);

foreach ($artisti as $artista) {
	$sql_artisti .= "partecipazioneArtista.nomeArte = ? AND ";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$artisti[array_search($artista, $artisti)];
	$artisti_join = true;
}
$sql_artisti = substr($sql_artisti, 0, -5);

if(strlen($ricerca) > 0){
	$ricerca = "%$ricerca%";
	$sql_ricerca .= "evento.titolo like ? OR evento.descrizione like ? ";
	$params_to_bind .= "ss";
	$params_to_bind_names[] = &$ricerca;
	$params_to_bind_names[] = &$ricerca;
}

//Date
if(strlen($dataInizio) > 0 && strlen($dataFine) > 0){
	$sql_data = "evento.dataEvento BETWEEN ? AND ?";
	$params_to_bind .= "ss";
	$params_to_bind_names[] = &$dataInizio;
	$params_to_bind_names[] = &$dataFine;
}

//Limit
$limit = " LIMIT ?, ?";
$offset = (strlen($pagina) && is_numeric($pagina)) ? ($pagina * $PAGE_LEN) : 0;

$params_to_bind .= 'ii';
$params_to_bind_names[] = &$offset;
$params_to_bind_names[] = &$PAGE_LEN;

$sql_where_params =
(strlen($sql_regione) 	> 0 ? (" AND " . $sql_regione) : "") .
(strlen($sql_provincia) > 0 ? (" AND " . $sql_provincia) : "") .
(strlen($sql_categorie) > 0 ? (" AND " . $sql_categorie) : "") .
(strlen($sql_artisti)   > 0 ? (" AND " . $sql_artisti) : "") .
(strlen($sql_ricerca)   > 0 ? (" AND " . $sql_ricerca) : "") .
(strlen($sql_data)   	> 0 ? (" AND " . $sql_data) : "");

$sql_where_params = substr($sql_where_params, strlen(" AND "));

$to_join = "";
if($categorie_join){
	$to_join .= " INNER JOIN categoriaEvento ON (evento.ID = categoriaEvento.IDEvento)";
}
if($artisti_join){
	$to_join .= " INNER JOIN partecipazioneArtista ON (evento.ID = partecipazioneArtista.IDEvento)";
}

$result = NULL;
if(strlen($params_to_bind) > 0 && count($params_to_bind_names) > 0){
	$sql = "SELECT evento.ID, evento.dataEvento, evento.titolo FROM evento $to_join" .
	(strlen($sql_where_params) > 0 ? " WHERE $sql_where_params" : '') .
	" ORDER BY evento.dataEvento DESC" .
	$limit;

	$stmt = $connessione->prepare($sql);
	$stmt->bind_param($params_to_bind, ...$params_to_bind_names);
}else{
	$stmt = $connessione->prepare("SELECT evento.ID, evento.dataEvento, evento.titolo FROM evento ORDER BY evento.dataEvento DESC $limit");
}
$result = $stmt->execute();

if($result === TRUE){
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()){
		echo "<a href=\"./Visualizza.php?ID=".$row["ID"]."\">". $row["titolo"] ."</a>";
	}
} else {
	echo "<p>Qualcosa e' andato stornto!</p>";
}
$connessione->close();

?>
