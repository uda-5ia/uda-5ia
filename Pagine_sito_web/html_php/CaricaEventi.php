<?php
$provincia        = (isset($_GET["provincia"])  &&	strlen($_GET['provincia'])  > 0) ? $_GET["provincia"]  : "";

$regione = (isset($_GET["regione"]) &&	strlen($_GET['regione']) > 0 && strlen($provincia) <= 0) ? urldecode($_GET['regione']) : '';

$categorie        = (
                      isset($_GET["categorie"])      &&	
                      (
                        is_array($_GET['categorie']) &&
                        count($_GET['categorie'])  > 0 &&
                        strlen($_GET['categorie'][0]) > 0
                      )
                    ) ?  $_GET["categorie"] : [];

$artisti          = (
                      isset($_GET["artisti"])      &&	
                      (
                        is_array($_GET['artisti']) &&
                        count($_GET['artisti'])  > 0 &&
                        strlen($_GET['artisti'][0]) > 0
                      )
                    ) ?  $_GET["artisti"] : [];
                    
$dataInizio       = (isset($_GET["dataInizio"]) &&	strlen($_GET['dataInizio']) > 0) ? $_GET["dataInizio"] : "";
$dataFine         = (isset($_GET["dataFine"])   &&	strlen($_GET['dataFine'])   > 0) ? $_GET["dataFine"]   : "";
$ricerca 	        = (isset($_GET["ricerca"])  	 && strlen($_GET['ricerca'])    > 0) ? $_GET["ricerca"]    : "";

require_once "ConnessioneDB.php";

$categorie_join = false;

$artisti_join = false;
$artisti_da_cercare = 0;

$params_to_bind = "";
$params_to_bind_names = [];

$sql_regione = "";
$sql_provincia = "";
$sql_categorie = "";
$sql_artisti = "";
$sql_data = "";
$sql_ricerca = "";

$order = 'DESC';
$now = date('Y-m-d H:i:s');

$offset = 0;

if(strlen($regione) > 0){
	$sql_regione = "evento.provinciaEvento IN (" .
		"SELECT provincia
		FROM provincePerRegione
		WHERE regione = ?
	)";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$regione;
}elseif(strlen($provincia) > 0){
	$sql_provincia = "evento.provinciaEvento = ?";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$provincia;
}

if(!empty($categorie) && is_array($categorie)){

	$sql_categorie = "categoriaEvento.nomeCategoria IN (";

	foreach ($categorie as $categoria) {
		$sql_categorie .= "?,  ";
		$params_to_bind .= "s";
		$params_to_bind_names[] = &$categorie[array_search($categoria, $categorie)];
		$categorie_join = true;
	}
	$sql_categorie = substr($sql_categorie, 0, -strlen('?, ')) . ')';
}
if(!empty($artisti) && is_array($artisti)){

	$sql_artisti = 'partecipazioneArtista.nomeArte in (';

	foreach ($artisti as $artista) {
		$sql_artisti .= "?, ";
		$params_to_bind .= "s";
		$params_to_bind_names[] = &$artisti[array_search($artista, $artisti)];
		$artisti_join = true;
	}
	
	$artisti_da_cercare = count($artisti);

	$sql_artisti = substr($sql_artisti, 0, -strlen(', ')) . ')';
	//$sql_artisti = substr($sql_artisti, 0, -strlen(' OR '));
}

if(strlen($ricerca) > 0){
	$ricerca = "%$ricerca%";
	$sql_ricerca .= "evento.titolo like ? OR evento.descrizione like ? ";
	$params_to_bind .= "ss";
	$params_to_bind_names[] = &$ricerca;
	$params_to_bind_names[] = &$ricerca;
}

//Date
if(strlen($dataInizio) > 0 && strlen($dataFine) > 0 && $dataInizio !== $dataFine){
	$sql_data = "evento.dataEvento BETWEEN ? AND ?";
	$params_to_bind .= "ss";
	$params_to_bind_names[] = &$dataInizio;
	$params_to_bind_names[] = &$dataFine;
	$order = 'ASC';
}

if(strlen($dataInizio) > 0 && strlen($dataFine) <= 0){
	$sql_data = "evento.dataEvento >= ?";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$dataInizio;
	$order = 'ASC';
}

if(strlen($dataFine) > 0 && strlen($dataInizio) <= 0){
	$sql_data = "evento.dataEvento BETWEEN ? AND ?";
	$params_to_bind .= "ss";
	$params_to_bind_names[] = &$now;
	$params_to_bind_names[] = &$dataFine;
	$order = 'ASC';
}

if(strlen($dataInizio) <= 0 && strlen($dataFine) <= 0){
	$sql_data = "evento.dataEvento >= ?";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$now;
	$order = 'ASC';
}

$sql_where_params =
(strlen($sql_regione) 	> 0 ? (" AND " . $sql_regione) : "") .
(strlen($sql_provincia) > 0 ? (" AND " . $sql_provincia) : "") .
(strlen($sql_categorie) > 0 ? (" AND " . $sql_categorie) : "") .
(strlen($sql_artisti)   > 0 ? (" AND " . $sql_artisti) : "") .
(strlen($sql_ricerca)   > 0 ? (" AND " . $sql_ricerca) : "") .
(strlen($sql_data)   	> 0 ? (" AND " . $sql_data) : "");

$sql_where_params = substr($sql_where_params, strlen(" AND "));

$to_join = "";
if($categorie_join){
	$to_join .= " INNER JOIN categoriaEvento ON (evento.ID = categoriaEvento.IDEvento)";
}
if($artisti_join){
	$to_join .= " INNER JOIN partecipazioneArtista ON (evento.ID = partecipazioneArtista.IDEvento)";
}

$result = NULL;
$select_from_evento = "SELECT DISTINCT evento.ID, evento.provinciaEvento, SUBSTRING(evento.descrizione, 1, 351) AS descrizione, evento.dataEvento, evento.titolo FROM evento";

$sql = "$select_from_evento $to_join" .
(strlen($sql_where_params) > 0 ? " WHERE $sql_where_params" : '') .
($artisti_join ? " GROUP BY partecipazioneArtista.IDEvento HAVING COUNT(partecipazioneArtista.nomeArte) >= $artisti_da_cercare" : '') .
" ORDER BY evento.dataEvento $order";

$stmt = $connessione->prepare($sql);
$stmt->bind_param($params_to_bind, ...$params_to_bind_names);

$result = $stmt->execute();

if($result === TRUE){
	$result = $stmt->get_result();

	while($row = $result->fetch_assoc()){
		echo "<a href=\"./Visualizza.php?ID=".$row["ID"]."\">". $row["titolo"] ."</a><br />";
	}
} else {
	echo '</p>Qualcosa è andato storto<p>';
}
$connessione->close();
?>
