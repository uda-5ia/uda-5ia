<?php
$servername = "";
$db_name = "";
$db_username = "";
$db_password = "";

$connessione = new mysqli($servername, $db_username, $db_password, $db_name);

if ($connessione->connect_error) {
  die("Connessione fallita");
}
$connessione->set_charset("utf8mb4_unicode_ci");

/*
Copia questo file e rimuovi la parte "_template"
e inserisci le credenziali del tuo server sql
*/

?>