<?php

session_start();
require_once("ConnessioneDB.php");

if(isset($_GET['ID'])){

  $ID = is_numeric(substr($_GET['ID'], 0, 32)) ? substr($_GET['ID'], 0, 32) : "none";

  if(is_numeric($ID) && $ID != "none"){

    if(isset($_SESSION["permessoUtente"])){

      if($_SESSION["permessoUtente"] != "S"){

        $eliminazione = isset($_POST['elimina']) ? $_POST['elimina'] : "";

        if($_SESSION["permessoUtente"] == "A" && $eliminazione == "evento" ){

          $sql = $connessione->prepare("DELETE FROM post WHERE IDEvento = ?");
          $sql->bind_param("i", $ID);
          $result = $sql->execute();

          $sql = $connessione->prepare("DELETE FROM partecipazioneArtista WHERE IDEvento = ?");
          $sql->bind_param("i", $ID);
          $result = $sql->execute();

          $sql = $connessione->prepare("DELETE FROM categoriaEvento WHERE IDEvento = ?");
          $sql->bind_param("i", $ID);
          $result = $sql->execute();

          $sql = $connessione->prepare("DELETE FROM evento WHERE ID = ?");
          $sql->bind_param("i", $ID);
          $result = $sql->execute();

          header("Location : ./Consulta.php");

        }else if(($_SESSION["permessoUtente"] == "A" || $_SESSION["permessoUtente"] == "M") && $eliminazione == "post"){

          $postID = is_numeric($_POST['id_post']) ? $_POST['id_post'] : "none" ;

          if(is_numeric($postID) && $postID != "none" ){
            $sql = $connessione->prepare("DELETE FROM post WHERE ID = ?");
            $sql->bind_param("i", $postID);
            $result = $sql->execute();
          }
        }
      }

    }

    $sql = $connessione->prepare("SELECT * FROM evento WHERE ID = ? LIMIT 1");
    $sql->bind_param("i", $ID);
    $result = $sql->execute();

    if($result === TRUE){
      $result = $sql->get_result()->fetch_assoc();
      if(! empty($result) && count($result) >= 1){
        if(isset($_POST['commento']) && isset($_POST['voto']) &&
        strlen($_POST['commento']) > 0 && strlen($_POST['voto']) > 0 &&
        is_numeric($_POST['voto']) &&
        $_POST['voto'] >= 1 && $_POST['voto'] <= 5){
        $data = date ('Y-m-d H:i:s');
          $sql = 'INSERT INTO post(dataCommento, commento, voto, nicknameUtente, IDEvento)
          VALUES (?, ?, ?, ?, ?)';
          $stmt = $connessione->prepare($sql);
          if($stmt !== FALSE){
            $stmt->bind_param(
              'ssisi',
              $data,
              $_POST['commento'],
              $_POST['voto'],
              $_SESSION['user_nickname'],
              $ID
            );
            if($stmt !== FALSE){
              if($stmt->execute() === TRUE){
                header("location:Visualizza.php?ID=$ID");
              }
            }
          }

        }

        ?>

        <html>
        <head>

          <title>Lista Eventi Community web</title>
          <link rel="stylesheet" href="./css/Visualizza.css">
          <meta charset="UTF-8"/>

        </head>

        <body>

          <h1>Titolo: <?php echo $result["titolo"];?></h1>
          <p>Data: <?php echo $result["dataEvento"];?></p>
          <p>Descrizione: <?php echo $result["descrizione"];?></p>
          <p>Provincia evento: <?php echo $result["provinciaEvento"];?></p>
          <p>Inserito da: <?php echo $result["nicknameMembro"];?></p>

          <?php

          if(isset($_SESSION["permessoUtente"])){

            if($_SESSION["permessoUtente"] != "S"){
              echo "<form method=\"post\" action=\"\" name=\"elimina\" >";

              if($_SESSION["permessoUtente"] == "A"){

                echo "<input type=\"radio\" name=\"elimina\" value=\"evento\"/>Elimina Evento";//DA FARE ELIMINAZIONE EVENTO
                echo "<br/><input type=\"radio\" name=\"elimina\" value=\"post\"/>Elimina post ";//DA FARE ELIMINAZIONE COMMENTO
                echo "<br/>Numero Post<input type=\"number\" name=\"id_post\" />";

              }else if($_SESSION["permessoUtente"] == "M"){

                echo "Elimina post: <input type=\"radio\" name=\"elimina\" value=\"post\"/>";//DA FARE ELIMINAZIONE COMMENTO
                echo "Numero Post<input type=\"number\" name=\"id_post\" />";
              }

              echo "<input type=\"submit\" name=\"Eliminazione\" value=\"Eliminazione\" /></form>";
            }
          }


          if(isset($_SESSION['user_nickname'])){ ?>
            <form method = "post">
              <label for="commento">Commento*:</label>
              <input type="textarea" id="commento" name="commento"/>
              <label for="voto">Voto*:</label>
              <input type="number" min="1" max="5" id="voto" name="voto"/>
              <input type="submit" name="submit" value="Commenta" />
            </form>
          <?php }else { ?>
            <p>Devi <a href="./Login.php">Accedere</a> per commentare gli eventi</p>
          <?php } ?>
          <?php
          $stmt = $connessione->prepare(
            "SELECT ID, dataCommento, commento, voto, nicknameUtente
            FROM post WHERE IDEvento = ? ORDER BY dataCommento DESC"
          );
          if($stmt !== FALSE){
            if($stmt->bind_param("i", $ID) !== FALSE){

              if($stmt->execute() !== FALSE){
                $result = $stmt->get_result();
                if($result->num_rows > 0){
                  echo '<div>';
                  while($row = $result->fetch_assoc()){

                    echo "<strong>Utente: {$row['nicknameUtente']} il {$row['dataCommento']}</strong>";
                    echo "<p>Voto: {$row['voto']}</p>";
                    echo "<p>Commento: {$row['commento']}</p>";
                    if(isset($_SESSION["permessoUtente"]) && ($_SESSION["permessoUtente"] == "A" || $_SESSION["permessoUtente"] == "M")){
                      echo "<p>ID: {$row['ID']}</p>";
                    }
                  }
                  echo '</div>';
                }
              }
            }
          }
          ?>
        </body>
        </html>

        <?php
      }else{ ?>
        <h1 style="color:red;text-align:center;">Evento non trovato</h1>
      <?php
      }
    }else{
      header("HTTP/1.1 400 Bad Request");
      $connessione->close();
      die();
    }
  }else{
    header("HTTP/1.1 400 Bad Request");
    $connessione->close();
    die();
  }
}else{
  header("HTTP/1.1 400 Bad Request");
  $connessione->close();
  die();
}
$connessione->close();
?>
<a href="./Consulta.php">Indietro</a>
