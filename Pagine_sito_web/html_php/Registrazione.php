<?php
/*

Gestire eccezione niente provincia
Eccezione quando metti troppe categorie
Aosta non compre e altre non compaiono

*/

session_start();
if(isset($_SESSION['user_nickname'])){
  header('Location: Azione.php');
  exit;
}
require_once("ConnessioneDB.php");

$out = [];

if (isset($_POST['registrami']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['nome']) && isset($_POST['cognome']) && isset($_POST['email']) && isset($_POST['provincia']) && isset($_POST['categorie'])) {

  $nomeUtente = htmlentities(substr($_POST['username'], 0, 32));
  $password = htmlentities(substr(password_hash($_POST['password'], PASSWORD_DEFAULT), 0, 255));
  $nome = htmlentities(substr($_POST['nome'], 0, 32));
  $cognome = htmlentities(substr($_POST['cognome'], 0, 32));
  $email = substr(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL), 0, 320);
  $provincia = htmlentities(substr($_POST['provincia'], 0, 25));
  $categorie = $_POST['categorie'];

  if(
    $stmt === FALSE ||
    $stmt->bind_param("s", $nomeUtente) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE){
    $connessione->close();
    die();
  }

  if ($result->num_rows >= 1) {
    $out[] = "C'e gia un account con quel nome utente.";
  }else{

    if(
      $stmt === FALSE ||
      $stmt->bind_param("s", $email) === FALSE ||
      $stmt->execute() === FALSE ||
      ($result = $stmt->get_result()) === FALSE){
        $connessione->close();
        die();
    }

    if ($result->num_rows >= 1) {
      $out[] = "C'e gia un account con quell'email.";
    }else{

      $sql = $connessione->prepare("INSERT INTO membro (nickname, nome, cognome, email, password, provinciaDiResidenza) VALUES (?, ?, ?, ?, ?, ?)");
      $sql->bind_param("ssssss", $nomeUtente, $nome, $cognome, $email, $password, $provincia);
      $result = $sql->execute();
      #$errcat = [];

      if ($result === TRUE) {
        $sql = $connessione->prepare("INSERT INTO interesse (nicknameMembro, nomeCategoria) VALUES (?, ?)");
        $sql->bind_param("ss", $nomeUtente, $gen);
        foreach ($categorie as $categoria) {
          $gen = htmlspecialchars(substr($categoria, 0, 64));
          $result = $sql->execute();
          #$errcat[] = $gen;
          if($result === false){
            break;
          }
        }

        if ($result === TRUE) {
          $_SESSION['user_nickname'] = $nomeUtente;
		      $_SESSION['permessoUtente'] = 'S';
          header('Location: Azione.php');
          $connessione->close();
          die();
        } else {
          $out[] = "<p>Qualcosa e' andato storto nell'inserimento degli interessi!</p>";


          /*foreach($errcat as $err) {
          echo $err; //debug
        }*/
        $sql = $connessione->prepare("DELETE FROM interesse WHERE nicknameMembro = ?");
        $sql->bind_param("s", $nomeUtente);
        $result = $sql->execute();

        $sql = $connessione->prepare("DELETE FROM membro WHERE nickname = ?");
        $sql->bind_param("s", $nomeUtente);
        $result = $sql->execute();

      }
    } else {
      $out[] = "<p>Qualcosa e' andato storto nell'inserimento dell'utente nel DB!</p>";
    }
  }
}
if(count($out) <= 0){
  $connessione->close();
  die();
}
}

?>
<html>
<head>

  <title>Registrazione Community web</title>
  <link rel="stylesheet" href="./css/Registrazione.css">
  <meta charset="UTF-8"/>
</head>

<body>

  <form method="post" action="" name="signup-form">
    <div class = "center">
      <div class="input">
        <label>Nome*</label>
        <input type="text" name="nome" value = "<?php echo isset($nome) ? $nome : ""; ?>" maxlength="32" required />
      </div>
      <br>
      <div class="input">
        <label>Cognome*</label>
        <input type="text" name="cognome" value = "<?php echo isset($cognome) ? $cognome : ""; ?>" maxlength="32" required />
      </div>
      <br>
      <div class="input">
        <label>Email*</label>
        <input type="email" name="email" value = "<?php echo isset($email) ? $email : ""; ?>" maxlength="320" required />
      </div>
      <br>
      <div class="input">
        <label>Nome utente*</label>
        <input type="text" name="username" value = "<?php echo isset($nomeUtente) ? $nomeUtente : ""; ?>" maxlength="32" required />
      </div>
      <br>
      <div class = "input">
        <label>Password*</label>
        <input type="password" name="password" maxlength="255" required />
      </div>
      <br>
      <div class = "inputCategoria">
        <h3>Categorie*</h3>
        <?php

        $sql = $connessione->prepare("SELECT * FROM categoria ORDER BY tipologia, nome ASC");
        $result = $sql->execute();
        $tip_cat = [];
        if($result === TRUE){

          $result = $sql->get_result();

          while($row = $result->fetch_array()){
            $cat = $row[0];
            $tipologia = $row[1];
            if(!isset($tip_cat[$tipologia])){
              $tip_cat[$tipologia] = [];
            }
            $tip_cat[$tipologia][] = $cat;
          }

          $id = 0;
          foreach ($tip_cat as $tipologia => $cats) {
            echo "<h4>$tipologia</h4>";
            foreach ($cats as $cat) {
              echo "<div class = \"input\"><label for = '$id'>$cat</label><input type=\"checkbox\" id = '$id' name=\"categorie[]\" value=\"".$cat."\"/></div>";
              $id++;
            }
          }

        } else {
          echo "<p>Qualcosa e' andato stornto!</p>";
        }

        ?>
      </div>
      <div class="input">
        <label for = "regione_select">Regione*</label>
        <select id = "regione_select" required>
          <option value = ""></option>
          <?php
          $sql = $connessione->prepare("SELECT DISTINCT regione FROM provincePerRegione ORDER BY regione ASC");
          $result = $sql->execute();
          if($result === TRUE){

            $result = $sql->get_result();

            while($row = $result->fetch_array()){

              echo "<option value = '$row[0]'>" . $row[0] . "</option>";

            }

          } else {
            echo "<p>Qualcosa e' andato stornto!</p>";
          }
          ?>
        </select>
        <div id = "div_provincia" style = "display: none;">
          <label for="provincia_select">Provincia*</label>
          <select id = "provincia_select" name = "provincia" required>
          </select>
        </div>
      </div>
      <button type="submit" name="registrami" value="registrami">Registrami</button>
    </div>
  </form>
  <a href="./Azione.php">Indietro</a>
  <a href="./Login.php">Login</a>

  <?php
  foreach($out as $str){
    echo $str;
  }
  ?>

  <script type="text/javascript" src="./js/Province.js"></script>
</body>
<?php
$connessione->close();
?>
</html>
