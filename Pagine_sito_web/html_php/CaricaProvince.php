<?php
session_start();
if(!isset($_GET["regione"]) || strlen($_GET["regione"]) <= 0){
	die();
}
$regione = urldecode($_GET["regione"]);

require_once("ConnessioneDB.php");

$sql = $connessione->prepare(
	"SELECT provincia FROM provincePerRegione WHERE regione = (?) ORDER BY provincia ASC"
);
$sql->bind_param("s", $regione);
$result = $sql->execute();

if($result === TRUE){
	$result = $sql->get_result();
	echo "<option value = ''></option>";
	while($row = $result->fetch_array()){
		echo "<option value = '" . htmlspecialchars($row[0]) . "'>" . htmlspecialchars($row[0]) . "</option>";
	}
} else {
	echo "<p>Qualcosa e' andato stornto!</p>";
}
$connessione->close();
?>
