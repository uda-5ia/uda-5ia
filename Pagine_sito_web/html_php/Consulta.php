<?php

session_start();
require_once("ConnessioneDB.php");

?>

<html>
<head>

  <title>Lista Eventi Community web</title>
  <link rel="stylesheet" href="./css/Consulta.css">
  <meta charset="UTF-8"/>
</head>

<body>

  <div class = "filtri_base">
    <div class="input">
      <label for = "regione_select">Regione*</label>
      <select id = "regione_select">
        <option value = ""></option>
        <?php
        $sql = $connessione->prepare("SELECT DISTINCT regione FROM provincePerRegione ORDER BY regione ASC");
        $result = $sql->execute();
        if($result === TRUE){

          $result = $sql->get_result();

          while($row = $result->fetch_array()){

            echo "<option value = '" . urlencode($row[0]) . "'>" . htmlspecialchars($row[0]) . "</option>";

          }

        } else {
          echo "<p>Qualcosa e' andato stornto!</p>";
        }
        ?>
      </select>
      <div id = "div_provincia" style = "display: none;">
        <label for="provincia_select">Provincia*</label>
        <select id = "provincia_select" name = "provincia">
        </select>
      </div>
    </div>
    <div class = "inputCategoria">
      <h3>Categorie*</h3>
      <?php

      $sql = $connessione->prepare("SELECT * FROM categoria ORDER BY tipologia, nome ASC");
      $result = $sql->execute();
      $tip_cat = [];
      if($result === TRUE){

        $result = $sql->get_result();

        while($row = $result->fetch_array()){
          $cat = $row[0];
          $tipologia = $row[1];
          if(!isset($tip_cat[$tipologia])){
            $tip_cat[$tipologia] = [];
          }
          $tip_cat[$tipologia][] = $cat;
        }

        $id = 0;
        foreach ($tip_cat as $tipologia => $cats) {
          echo "<h4>$tipologia</h4>";
          foreach ($cats as $cat) {
            echo "<div class = \"input\"><label for = '$id'>$cat</label><input type=\"checkbox\" id = '$id' name=\"categorie\" value=\"".$cat."\"/></div>";
            $id++;
          }
        }

      } else {
        echo "<p>Qualcosa e' andato stornto!</p>";
      }

      ?>
      <br/><br/>
    </div>

  </div>
  <div class = "filtri_avanzati">
    <div class="input">
      <label>Data inizio*</label>
      <input type="date" id="dataInizio" required />
    </div>
    <div class="input">
      <label>Data fine*</label>
      <input type="date" id="dataFine" required />
    </div>
    <div class = "input">
      <label>Artista*</label>
      <?php

      $sql = $connessione->prepare("SELECT * FROM artista ORDER BY nomeArte, nome, cognome ASC");
      $result = $sql->execute();
      if($result === TRUE){

        $result = $sql->get_result();

        $id=0;
        while($row = $result->fetch_array()){
          echo "<div class = \"input\"><label for = '$id'>".$row[0]."(".$row[1].", ".$row[2].")</label><input type=\"checkbox\" id = '$id' name=\"artisti\" value=\"".$row[0]."\"/></div>";
        }

      } else {
        echo "<p>Qualcosa e' andato stornto!</p>";
      }

      ?>
    </div>
  </div>
  <div id = "lista_eventi">

  </div>

  <a href="./Azione.php">Indietro</a>

  <?php if(!isset($_SESSION['user_nickname'])) : ?>
    <a href="./Login.php">Login</a>
  <?php endif; ?>

  <script type="text/javascript" src="./js/Province.js"></script>
  <script type="text/javascript" src="./js/Eventi.js"></script>
</body>
</html>
