window.addEventListener("load", windowLoaded());

function windowLoaded(){
	let select = document.getElementById("regione_select");
	if(select){
		select.addEventListener("change", caricaProvince);
	}
}

function caricaProvince(e){
	let select_provincia = document.getElementById("provincia_select");
	if(select_provincia){
		select_provincia.innerHTML = "";
	}
	fetch("./CaricaProvince.php?regione=" + e.target.value,
	{}).then(response => {
		return response.text();
	}).then(text => {
		let select_provincia = document.getElementById("provincia_select");
		if(select_provincia){
			select_provincia.innerHTML = text;
			document.getElementById("div_provincia").style.display = "block";
		}
	});
}
