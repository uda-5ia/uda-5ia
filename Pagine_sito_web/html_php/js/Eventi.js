let regione, categorieAttive, provincia, dataInizio, dataFine, artistiAttive;
window.addEventListener("load", windowLoaded());

function windowLoaded(){
    regione = document.getElementById("regione_select");
	if(regione){
		regione.addEventListener("change", caricaEventi);
	}
    provincia = document.getElementById("provincia_select");
	if(provincia){
		provincia.addEventListener("change", caricaEventi);
	}
    let categorie = document.querySelectorAll("input[type=checkbox][name=categorie]");
    categorieAttive = [];
    categorie.forEach(function(checkbox) {
      checkbox.addEventListener('change', function() {
        categorieAttive = Array.from(categorie).filter(i => i.checked).map(i => i.value);
        //console.log(categorieAttive)
        caricaEventi();
      })
    });
    dataInizio = document.getElementById("dataInizio");
	dataFine = document.getElementById("dataFine");
	if(dataInizio){
		dataInizio.addEventListener("change", function(){
            if(dataFine.value == ""){
                dataFine.value = dataInizio.value.split("-")[0] + "-" + dataInizio.value.split("-")[1] + "-" + (parseInt(dataInizio.value.split("-")[2])+1);
            }
            caricaEventi();
        });
	}
	if(dataFine){
		dataFine.addEventListener("change", caricaEventi);
	}
    let artisti = document.querySelectorAll("input[type=checkbox][name=artisti]");
    artistiAttive = [];
    artisti.forEach(function(checkbox) {
      checkbox.addEventListener('change', function() {
        artistiAttive = Array.from(artisti).filter(i => i.checked).map(i => i.value);
        //console.log(artistiAttive)
        caricaEventi();
      })
    });
	caricaEventi();
}

function caricaEventi(e){

    let regioneRequest = "regione=" + encodeURIComponent(regione.value);
    let provinciaRequest = "provincia=" + encodeURIComponent(provincia.value);
    let categoriaRequest = function(){
        let result = "";
        categorieAttive.forEach((cat) => {
            result += "categorie[]=" + encodeURIComponent(cat);
            if(cat != categorieAttive[categorieAttive.length - 1]){
                result += "&";
            }
        });
        return result;
    };
    let dataInizioRequest = "dataInizio=" + encodeURIComponent(dataInizio.value);
    let dataFineRequest = "dataFine=" + encodeURIComponent(dataFine.value);
    let artistiRequest = function(){
        let result = "";
        artistiAttive.forEach((art) => {
            result += "artisti[]=" + encodeURIComponent(art);
            if(art != artistiAttive[artistiAttive.length - 1]){
                result += "&";
            }
        });
        return result;
    };

	fetch("./CaricaEventi.php?"+regioneRequest+"&"+provinciaRequest+"&"+categoriaRequest()+"&"+dataInizioRequest+"&"+dataFineRequest+"&"+artistiRequest(),
	{}).then(response => {
		return response.text();
	}).then(text => {
		let lista = document.getElementById("lista_eventi");
        lista.innerHTML = text;
	});
}
