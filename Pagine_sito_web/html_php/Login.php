<?php

session_start();
if(isset($_SESSION['user_nickname'])){
  header('Location: Azione.php');
  exit;
}

$out = [];

if (isset($_POST['login']) && isset($_POST['username']) && isset($_POST['password'])) {
  require_once("ConnessioneDB.php");

  $nomeUtente = htmlentities(substr($_POST['username'], 0, 32));
  $password = substr($_POST['password'], 0, 255);

  $sql = $connessione->prepare("SELECT * FROM membro WHERE nickname = ?");
  $sql->bind_param("s", $nomeUtente);
  $result = $sql->execute();

  if($result === TRUE){

    $result = $sql->get_result()->fetch_assoc();

    if ($result >= 1) {

      if(password_verify($password, $result['password'])){

        $_SESSION['user_nickname'] = $result['nickname'];
        $_SESSION['permessoUtente'] = $result['tipo'];
        header('Location: Azione.php');

      }else{

        $out[] = "Password sbagliata";

      }
    }else{

      $out[] =  "Nome utente non trovato.";

    }
  }
  $connessione->close();
}

?>
<html>
<head>

  <title>Login Community web</title>
  <link rel="stylesheet" href="./css/Accesso.css">

</head>

<body>

  <form method="post" action="" name="signin-form">
    <div class = "center">
      <div class="input">
        <label>Nome Utente</label>
        <input type="text" name="username" value = "<?php echo isset($nomeUtente) ? $nomeUtente : ""; ?>" maxlength="32" required />
      </div>
      <br>
      <div class = "input">
        <label>Password</label>
        <input type="password" name="password" maxlength="255" required />
      </div>
      <br>
      <button type="submit" name="login" value="login">Login</button>
    </div>
  </form>

  <a href="./Azione.php">Indietro</a>
  <a href="./Registrazione.php">Registrati</a>

  <?php
  foreach($out as $str){
    echo $str;
  }
  ?>
</body>
</html>