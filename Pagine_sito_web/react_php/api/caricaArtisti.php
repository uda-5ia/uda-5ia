<?php
require_once "util.php";
maybe_start_session();
require_once "connessioneDB.php";

$sql = $connessione->prepare('SELECT * FROM artista');

$result = $sql->execute();
if($result === TRUE){
  $result = $sql->get_result();
  $artisti = [];
  if($result && $result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $artisti[] = [
		  'value' => htmlspecialchars($row['nomeArte']),
		  'label' => htmlspecialchars($row['nomeArte'] . ' (' . $row['nome'] . ' ' . $row['cognome'] . ')'),
	  ];
    }
  }

  send_json_success_options($artisti);
} else {
  send_json_error('Qualcosa è andato storto');
}

close_conn_and_die();
?>