<?php
require_once "util.php";
maybe_start_session();
require_once "connessioneDB.php";

$sql = $connessione->prepare("SELECT * FROM categoria ORDER BY tipologia, nome ASC");
$result = $sql->execute();
$tip_cat = [];
if($result === TRUE){
	$result = $sql->get_result();
	$result = $result->fetch_all(MYSQLI_ASSOC);
	$tip_cat_index = 0;
	$prev_tip = $result[0]['tipologia'];
	foreach($result as $res){
		$tipologia = htmlspecialchars($res['tipologia']);
		if($tipologia !== $prev_tip){
			$tip_cat_index++;
		}
		$prev_tip = $tipologia;

		$cat = htmlspecialchars($res['nome']);

		if(!isset($tip_cat[$tip_cat_index])){
			$tip_cat[] = [
				'label' => $tipologia,
				'options' => [],
			];
		}
		$tip_cat[$tip_cat_index]['options'][] = [
            'value' => $cat,
            'label' => $cat,
        ];
	}

	send_json_success_options($tip_cat, false);
} else {
	send_json_error('Qualcosa è andato storto');
}

close_conn_and_die();
?>
