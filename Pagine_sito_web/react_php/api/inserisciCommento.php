<?php
require_once "util.php";
session_start();

if(! is_user_logged_in() ){
  send_json_error('Utente non loggato');
  die();
}

if(
  ( 
    ! isset($_POST['voto']) ||
      strlen($_POST['voto']) <= 0 ||
    ! is_numeric($_POST['voto'])
  ) ||
  ( 
    ! isset($_POST['idEvento']) ||
      strlen($_POST['idEvento']) <= 0 ||
    ! is_numeric($_POST['idEvento'])
  )
){
  send_json_error('Qualcosa è andato storto');
  die();
}

$commento   = isset($_POST['commento']) ? $_POST['commento'] : '';

$voto       = $_POST['voto'];
$voto       = min(max($voto, 1), 5);

$idEvento   = $_POST['idEvento'];
$nomeUtente = user_nickname();
$data       = date('Y-m-d H:i:s', time());

require_once "connessioneDB.php";

$stmt = $connessione->prepare("SELECT ID FROM evento WHERE ID = ?");
$result = NULL;
if(
  $stmt === FALSE ||
  $stmt->bind_param("i", $idEvento) === FALSE ||
  $stmt->execute() === FALSE ||
  ($result = $stmt->get_result()) === FALSE ||
  $result->num_rows !== 1
){
  if($result->num_rows <= 0){
    send_json_error('Evento non trovato');
  }else{
    send_json_error('Qualcosa è andato storto');
  }
  close_conn_and_die();
}

$stmt = $connessione->prepare(
  "INSERT INTO post (dataCommento, commento, voto, nicknameUtente, IDEvento)
   VALUES (?, ?, ?, ?, ?)"
);

if(
  $stmt === FALSE ||
  $stmt->bind_param("ssisi", $data, $commento, $voto, $nomeUtente, $idEvento) === FALSE ||
  $stmt->execute() === FALSE
){
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}

send_json_success(['added' => true]);
close_conn_and_die();
?>
