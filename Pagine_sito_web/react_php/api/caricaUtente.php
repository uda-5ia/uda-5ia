<?php
  require_once "util.php";

  session_start();

  if( ! is_user_logged_in()){
    send_json_error('Utente non loggato');
    die();
  }

  $nomeUtente = user_nickname();

  require_once "connessioneDB.php";

  $stmt = $connessione->prepare("SELECT * FROM membro WHERE nickname = ?");
  $result = NULL;
  if(
    $stmt === FALSE ||
    $stmt->bind_param("s", $nomeUtente) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE ||
    $result->num_rows !== 1
  ){
    send_json_error('Qualcosa è andato storto');
    close_conn_and_die();
  }

  $user = $result->fetch_assoc();

  $stmt = $connessione->prepare("SELECT nomeCategoria FROM interesse WHERE nicknameMembro = ?");
  if(
    $stmt === FALSE ||
    $stmt->bind_param("s", $nomeUtente) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE
  ){
    send_json_error('Qualcosa è andato storto');
    close_conn_and_die();
  }

  $interesse_utente = [];
  while($interesse = $result->fetch_array()){
    $interesse_utente[] = htmlspecialchars($interesse[0]);
  }

  /*$tipo = user_type();*/
  send_json_success([
        'user_nickname' => htmlspecialchars($nomeUtente),
        /*'permessoUtente' => htmlspecialchars($tipo),*/
        'nome' => $user['nome'],
        'cognome' => $user['cognome'],
        'email' => $user['email'],
        'provincia' => $user['provinciaDiResidenza'],
        'categorie' => $interesse_utente,
        /*'sessionId' => session_id(),*/
    ]
  );
  close_conn_and_die();
?>
