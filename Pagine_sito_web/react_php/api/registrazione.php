<?php
/*

Gestire eccezione niente provincia
Eccezione quando metti troppe categorie
Aosta non compre e altre non compaiono

*/
require_once "util.php";

if(is_user_logged_in()){
  die();
}

maybe_start_session();

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['nome']) && isset($_POST['cognome']) && isset($_POST['email']) && isset($_POST['provincia']) && isset($_POST['categorie'])) {
  require_once "connessioneDB.php";

  $nomeUtente = htmlentities(substr($_POST['username'], 0, 32));
  $password = htmlentities(substr(password_hash($_POST['password'], PASSWORD_DEFAULT), 0, 255));
  $nome = htmlentities(substr($_POST['nome'], 0, 32));
  $cognome = htmlentities(substr($_POST['cognome'], 0, 32));
  $email = substr(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL), 0, 320);
  $provincia = htmlentities(substr($_POST['provincia'], 0, 25));
  $categorie = $_POST['categorie'];

  $result = NULL;
  $stmt = $connessione->prepare("SELECT nickname FROM membro WHERE nickname = ?");
  if(
    $stmt === FALSE ||
    $stmt->bind_param("s", $nomeUtente) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE){
    close_conn_and_die();
  }

  if ($result->num_rows >= 1) {
    send_json_error("C'è già un account con quel nome utente.");
    close_conn_and_die();
  }else{

    $stmt = $connessione->prepare("SELECT email FROM membro WHERE email = ?");
    $result = NULL;
    if(
      $stmt === FALSE ||
      $stmt->bind_param("s", $email) === FALSE ||
      $stmt->execute() === FALSE ||
      ($result = $stmt->get_result()) === FALSE){
        send_json_error('Qualcosa è andato storto');
        close_conn_and_die();
    }

    if ($result->num_rows >= 1) {
      send_json_error("C'e gia un account con quell'email.");
    }else{
      $stmt = $connessione->prepare("INSERT INTO membro (nickname, nome, cognome, email, password, provinciaDiResidenza) VALUES (?, ?, ?, ?, ?, ?)");
      $result = NULL;
      if(
        $stmt === FALSE ||
        $stmt->bind_param("ssssss", $nomeUtente, $nome, $cognome, $email, $password, $provincia) === FALSE ||
        ($result = $stmt->execute()) === FALSE){
          send_json_error('Qualcosa è andato storto');
          close_conn_and_die();
      }

      if ($result === TRUE) {
        $result = NULL;
        $stmt = $connessione->prepare("INSERT INTO interesse (nicknameMembro, nomeCategoria) VALUES (?, ?)");
        if( $stmt !== FALSE && $stmt->bind_param("ss", $nomeUtente, $gen) !== FALSE){
          foreach ($categorie as $categoria) {
            $gen = htmlspecialchars(substr($categoria, 0, 64));
            $result = $stmt->execute();

            if($result === false){
              break;
            }
          }
        }

        if ($result === TRUE) {
          $_SESSION['user_nickname'] = $nomeUtente;
		      $_SESSION['permessoUtente'] = 'S';
          send_json_success(['success' => true]);
          close_conn_and_die();
        } else {
          send_json_error("Qualcosa è andato storto nell'inserimento degli interessi!");
          $sql = $connessione->prepare("DELETE FROM interesse WHERE nicknameMembro = ?");
          $sql->bind_param("s", $nomeUtente);
          $result = $sql->execute();

          $sql = $connessione->prepare("DELETE FROM membro WHERE nickname = ?");
          $sql->bind_param("s", $nomeUtente);
          $result = $sql->execute();
          close_conn_and_die();
        }
      } else {
        send_json_error("Qualcosa è andato storto nell'inserimento dell'utente nel DB!");
        close_conn_and_die();
      }
    }
  }
  close_conn_and_die();
}
?>
