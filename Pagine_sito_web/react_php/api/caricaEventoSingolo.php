<?php
  require_once "util.php";

  maybe_start_session();

  if( ! ( isset($_POST['id']) && strlen($_POST['id']) > 0 && is_numeric($_POST['id'])) ){
    send_json_error('Qualcosa è andato storto');
    die();
  }
  $PAGE_LEN = comments_page_len();

  $id = $_POST['id'];
  $offset = 0;

  require_once "connessioneDB.php";

  $stmt = $connessione->prepare(
    "SELECT dataEvento, titolo,	descrizione, provinciaEvento,	nicknameMembro FROM evento WHERE ID = ?"
  );
  $result = NULL;
  if(
    $stmt === FALSE ||
    $stmt->bind_param("i", $id) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE ||
    $result->num_rows !== 1
  ){
    if($result->num_rows <= 0){
      send_json_error('Evento non trovato');
    }else{
      send_json_error('Qualcosa è andato storto');
    }
    close_conn_and_die();
  }

  $evento = $result->fetch_assoc();

  $stmt = $connessione->prepare(
    "SELECT nomeCategoria FROM categoriaEvento WHERE IDEvento = ?"
  );

  if(
    $stmt === FALSE ||
    $stmt->bind_param("i", $id) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE
  ){
    send_json_error('Qualcosa è andato storto');
    close_conn_and_die();
  }

  $categorie = [];
  while($categoria = $result->fetch_array()){
    $categorie[] = htmlspecialchars($categoria[0]);
  }

  $stmt = $connessione->prepare(
    "SELECT nomeArte FROM partecipazioneArtista WHERE IDEvento = ?"
  );

  if(
    $stmt === FALSE ||
    $stmt->bind_param("i", $id) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE
  ){
    send_json_error('Qualcosa è andato storto');
    close_conn_and_die();
  }

  $artisti = [];
  while($artista = $result->fetch_array()){
    $artisti[] = htmlspecialchars($artista[0]);
  }

  $stmt = $connessione->prepare(
    "SELECT ID, dataCommento, commento, voto, nicknameUtente
     FROM post WHERE IDEvento = ?
     ORDER BY dataCommento DESC
     LIMIT ?, ?"
  );
  $PAGE_LEN_TEST = $PAGE_LEN + 1;
  if(
    $stmt === FALSE ||
    $stmt->bind_param("iii", $id, $offset, $PAGE_LEN_TEST) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE
  ){
    send_json_error('Qualcosa è andato storto');
    close_conn_and_die();
  }
  
  $commenti = $result->fetch_all(MYSQLI_ASSOC);
  $altri_commenti = false;
  if(isset($commenti[$PAGE_LEN])){
    $altri_commenti = true;
    unset($commenti[$PAGE_LEN]);
  }

  /*Voto Medio*/
  $voto_medio = 0;
  $stmt = $connessione->prepare(
    "SELECT AVG(voto) as votoMedio
     FROM post WHERE IDEvento = ?
     GROUP BY IDEvento"
  );
  if(
    $stmt === FALSE ||
    $stmt->bind_param("i", $id) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE
  ){
    send_json_error('Qualcosa è andato storto');
    close_conn_and_die();
  }
  if($result->num_rows > 0){ 
    $result = $result->fetch_array();
    $voto_medio = $result[0];
  }
  /**/

  send_json_success([
        'nicknameMembro' => htmlspecialchars($evento['nicknameMembro']),
        'data'           => htmlspecialchars($evento['dataEvento']),
        'titolo'         => htmlspecialchars($evento['titolo']),
        'descrizione'    => htmlspecialchars($evento['descrizione']),
        'provincia'      => htmlspecialchars($evento['provinciaEvento']),
        'voto_medio'      => strval($voto_medio),
        'categorie'      => $categorie,
        'artisti'        => $artisti,
        'commenti'       => $commenti,
        'altri_commenti' => $altri_commenti,
    ]
  );
  close_conn_and_die();
?>
