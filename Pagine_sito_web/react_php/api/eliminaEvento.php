<?php
require_once "util.php";
session_start();

if( ! is_user_logged_in() || ! user_type() ){
  send_json_error('Utente non loggato');
  die();
}

$permessoUtente = user_type();

if($permessoUtente !== 'A'){
  send_json_error('Permesso negato');
  die();
}

if(
  ! isset($_POST['id'])       ||
    strlen($_POST['id']) <= 0 ||
  ! is_numeric($_POST['id'])
){
  send_json_error('Qualcosa è andato storto');
  die();
}

$id = $_POST['id'];

require_once "connessioneDB.php";

$stmt = $connessione->prepare("DELETE FROM post WHERE IDEvento = ?");
$result = NULL;
if(
  $stmt === FALSE ||
  $stmt->bind_param("i", $id) === FALSE ||
  $stmt->execute() === FALSE
){
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}

$stmt = $connessione->prepare("DELETE FROM partecipazioneArtista WHERE IDEvento = ?");
$result = NULL;
if(
  $stmt === FALSE ||
  $stmt->bind_param("i", $id) === FALSE ||
  $stmt->execute() === FALSE
){
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}

$stmt = $connessione->prepare("DELETE FROM categoriaEvento WHERE IDEvento = ?");
$result = NULL;
if(
  $stmt === FALSE ||
  $stmt->bind_param("i", $id) === FALSE ||
  $stmt->execute() === FALSE
){
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}

$stmt = $connessione->prepare("DELETE FROM evento WHERE ID = ?");
$result = NULL;
if(
  $stmt === FALSE ||
  $stmt->bind_param("i", $id) === FALSE ||
  $stmt->execute() === FALSE
){
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}

send_json_success(['removed' => true]);
close_conn_and_die();

?>