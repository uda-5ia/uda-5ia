<?php

require_once "util.php";
maybe_start_session();
require_once "connessioneDB.php";

$sql = $connessione->prepare(
	"SELECT * FROM provincePerRegione ORDER BY regione, provincia ASC"
);
$result = $sql->execute();

if($result === TRUE){
  $result = $sql->get_result();
	$result = $result->fetch_all(MYSQLI_ASSOC);
	$regione_index = 0;
	$prev_reg = $result[0]['regione'];
	foreach($result as $res){
		$regione = htmlspecialchars($res['regione']);
		if($regione !== $prev_reg){
			$regione_index++;
		}
		$prev_reg = $regione;

		$prov = htmlspecialchars($res['provincia']);

		if(!isset($reg_prov[$regione_index])){
			$reg_prov[] = [
				'label' => $regione,
				'options' => [],
			];
		}
		$reg_prov[$regione_index]['options'][] = [
            'value' => $prov,
            'label' => $prov,
        ];
	}

	send_json_success_options($reg_prov, false);
} else {
	send_json_error('Qualcosa è andato storto');
}

close_conn_and_die();
?>
