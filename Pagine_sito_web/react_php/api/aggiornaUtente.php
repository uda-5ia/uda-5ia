<?php
  require_once "util.php";
  session_start();

  if( ! is_user_logged_in()){
    send_json_error('Utente non loggato');
    die();
  }

  if(
      (!isset($_POST['provincia']) || strlen($_POST['provincia']) <= 0)  && 
      (!isset($_POST['email'])     || strlen($_POST['email'])     <= 0)  && 
      (!isset($_POST['categorie']) || count((array)$_POST['categorie']) <= 0)
  ){
    send_json_success(['modificato' => false]);
    die();
  }

  $nomeUtente = user_nickname();

  $nuovaProvincia = $_POST['provincia'] ?? FALSE;
  $nuovaEmail     = $_POST['email'] ?? FALSE;
  $nuoveCategorie = $_POST['categorie'] ?? FALSE;

  require_once "connessioneDB.php";

  function aggiungi_categorie_utente($categorie, $utente){

    global $connessione;

    $stmt = $connessione->prepare(
      "INSERT INTO interesse(nicknameMembro, nomeCategoria) VALUES(?, ?)"
    );
    $result = NULL;
    if($stmt === FALSE){
      return false;
    }
    $user = $utente;
    foreach($categorie as $categoria){
      $cat = htmlspecialchars($categoria);
      if(
        $stmt->bind_param("ss", $user, $cat) === FALSE ||
        $stmt->execute() === FALSE
      ){
        return false;;
      }
    }
    return true;
  }

  /*Just to make sure the user exists*/
  $stmt = $connessione->prepare("SELECT nickname FROM membro WHERE nickname = ?");
  $result = NULL;
  if(
    $stmt === FALSE ||
    $stmt->bind_param("s", $nomeUtente) === FALSE ||
    $stmt->execute() === FALSE ||
    ($result = $stmt->get_result()) === FALSE ||
    $result->num_rows !== 1
  ){
    send_json_error('Qualcosa è andato storto');
    close_conn_and_die();
  }

  if($nuovaProvincia){
    $stmt = $connessione->prepare("UPDATE membro SET provinciaDiResidenza = ? WHERE nickname = ?");
    if(
      $stmt === FALSE ||
      $stmt->bind_param("ss", $nuovaProvincia, $nomeUtente) === FALSE ||
      $stmt->execute() === FALSE
    ){
      send_json_error('Qualcosa è andato storto');
      close_conn_and_die();
    }
  }

  if($nuovaEmail){
    $stmt = $connessione->prepare("UPDATE membro SET email = ? WHERE nickname = ?");
    if(
      $stmt === FALSE ||
      $stmt->bind_param("ss", $nuovaEmail, $nomeUtente) === FALSE ||
      $stmt->execute() === FALSE
    ){
      send_json_error('Qualcosa è andato storto');
      close_conn_and_die();
    }
  }

  if($nuoveCategorie){
    $vecchieCategorie = [];
    $stmt = $connessione->prepare("SELECT nomeCategoria FROM interesse WHERE nicknameMembro = ?");
    if(
      $stmt === FALSE ||
      $stmt->bind_param("s", $nomeUtente) === FALSE ||
      $stmt->execute() === FALSE ||
      ($result = $stmt->get_result()) === FALSE
    ){
      send_json_error('Qualcosa è andato storto');
      close_conn_and_die();
    }
    $result = $result->fetch_array();
    if(is_array($result) && count($result) > 0){
      foreach($result as $cat){
        $vecchieCategorie[] = $cat;
      }
    }

    $stmt = $connessione->prepare("DELETE FROM interesse WHERE nicknameMembro = ?");
    if(
      $stmt === FALSE ||
      $stmt->bind_param("s", $nomeUtente) === FALSE ||
      $stmt->execute() === FALSE
    ){
      send_json_error('Qualcosa è andato storto');
      close_conn_and_die();
    }
    
    if( ! aggiungi_categorie_utente($nuoveCategorie, $nomeUtente)){
      $stmt = $connessione->prepare("DELETE FROM interesse WHERE nicknameMembro = ?");
      if(
        $stmt === FALSE ||
        $stmt->bind_param("s", $nomeUtente) === FALSE ||
        $stmt->execute() === FALSE
      ){
        if(count($vecchieCategorie) > 0){
          aggiungi_categorie_utente($vecchieCategorie, $nomeUtente);
        }
        send_json_error('Qualcosa è andato storto');
        close_conn_and_die();
      }
    }
  }
  send_json_success(['modificato' => true]);
  close_conn_and_die();
?>