<?php
require_once "util.php";

if (isset($_POST['username']) && isset($_POST['password'])) {
  session_start();

  if( !is_login_valid($_POST['username']) ){
    send_json_error("Qualcosa è andato storto");
    die();  
  }
  
  if(is_user_logged_in()){
    send_json_success([
          'user_nickname' => user_nickname(),
          'permessoUtente' => user_type(),
          'sessionId' => session_id(),
      ],
      false
    );
    die();
  }

  require_once "connessioneDB.php";

  $login = htmlentities(substr($_POST['username'], 0, 32));
  $password = substr($_POST['password'], 0, 255);

  $login_type = strpos($login, '@') !== FALSE ? 'email' : 'nickname';
  $sql_where  = " $login_type = ?";

  $stmt = $connessione->prepare("SELECT * FROM membro WHERE $sql_where");
  $stmt->bind_param("s", $login);
  $result = $stmt->execute();

  if($result != FALSE){

    $result = $stmt->get_result();

    if($result->num_rows > 0){

      $result = $result->fetch_assoc();

      if(password_verify($password, $result['password'])){

        $_SESSION['user_nickname']  = htmlspecialchars($result['nickname']);
        $_SESSION['permessoUtente'] = htmlspecialchars($result['tipo']);

      }else{
        send_json_error("Password invalida!");
        close_conn_and_die();
      }
    }else{
      send_json_error("Utente non trovato!");
      close_conn_and_die();
    }
  }

  close_conn();

  if(is_user_logged_in()){
    send_json_success([
        'user_nickname' => user_nickname(),
        'permessoUtente' => user_type(),
        'sessionId' => session_id(),
      ],
      false
    );
  }else{
    send_json_error("Qualcosa è andato storto");
  }
  close_conn_and_die();
}
?>
