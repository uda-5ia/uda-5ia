<?php
include_once 'settings.php';

$user_regex  = "/^[a-zA-Z0-9\!\_\+\-\*\#\$\^\.]+$/";
$mail_regex  = "/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/";
$strong_pass = "/^(?=.*[A-Z])(?=.*[\-\_\<\>\!\@\#\$\%\^\&\*\.])(?=.*[0-9])(?=.*[a-z]).{8,64}$/";
$medium_pass = "/^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,64}$/";

$sess_id = FALSE;
if(!defined('NO_REACT_HEADERS')){
  /*ini_set('session.cookie_secure', "1");
  ini_set('session.cookie_httponly', "1");
  ini_set('session.cookie_samesite', 'None');*/
  header('Access-Control-Allow-Origin: ' . (defined('DEBUG') ? '*' : ($_SERVER['HTTP_ORIGIN'] ?? $_SERVER['HTTP_REFERER'] ?? '*')));
  header('Access-Control-Allow-Credentials: true');
  header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type");
  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))

        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
  }
  $rest_json = file_get_contents("php://input");
  if(isset($rest_json) && strlen($rest_json) > 0){
    $_POST = json_decode($rest_json, true);
  }elseif(defined('DEBUG')){
    $_POST = $_REQUEST;
  }

  $sess_id = isset($_POST['sessionId']) ? $_POST['sessionId'] : FALSE;
  if($sess_id){
    session_id($sess_id);
  }
}

function maybe_start_session(){
  global $sess_id;
  if (
      $sess_id &&
      session_status() === PHP_SESSION_NONE
    ) {
    session_start();
  }
}

function array_htmlspecialchars($array){
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = htmlspecialchars($item);
        }
    });

    return $array;
}

function close_conn(){
  if(isset($connessione)){
    $connessione->close();
  }
}

function close_conn_and_die(){
  close_conn();
  die();
}

function send_json_error($msg){
  echo json_encode([
    'success' => false,
    'data' => [
      'msg' => htmlspecialchars($msg),
    ]
  ]);
}

function send_json_success($data, bool $encode = true){
  echo json_encode([
    'success' => true,
    'data' => [
      (isset($encode) && $encode === TRUE) ? (
        is_array($data) ? array_htmlspecialchars($data) : htmlspecialchars($data)
      ) : $data,
    ]
  ]);
}

function send_json_success_options($options, bool $encode = true){
  send_json_success(['options' => $options], $encode);
}

function is_username_valid($usr){
  global $user_regex;
  return (bool) preg_match($user_regex, $usr);
}

function is_mail_valid($mail){
  global $mail_regex;
  return (bool) preg_match($mail_regex, $mail);
}

function is_login_valid($login){
  return is_username_valid($login) || is_mail_valid($login);
}

function pass_strength($pass){
  global $medium_pass, $strong_pass;
  $strength = 0;
  $strength = preg_match($medium_pass, $pass) ? 1 : 0;
  $strength = preg_match($strong_pass, $pass) ? 2 : 0;
  return $strength;
}

function is_user_logged_in(){
  return session_status() === PHP_SESSION_ACTIVE && isset($_SESSION['user_nickname']);
}

function user_type(){
  if( ! is_user_logged_in() ){
    return false;
  }

  return $_SESSION['permessoUtente'];
}

function user_nickname(){
  if( ! is_user_logged_in() ){
    return false;
  }

  return $_SESSION['user_nickname'];
}

function comments_page_len(){
  return 10;
}

?>
