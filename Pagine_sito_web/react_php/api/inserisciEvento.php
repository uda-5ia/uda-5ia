<?php
require_once "util.php";
session_start();

if(! is_user_logged_in() ){
  send_json_error('Utente non loggato');
  die();
}

$nomeUtente = user_nickname();

if(
  ( ! isset($_POST['titolo'])      || strlen($_POST['titolo'])      <= 0 )       ||
  ( ! isset($_POST['provincia'])   || strlen($_POST['provincia'])   <= 0 )       ||
  ( 
    ! isset($_POST['data'])        ||
      strlen($_POST['data']) <= 0  ||
      DateTime::createFromFormat('Y-m-d H:i:s', $_POST['data']) === FALSE
  )                                                                              ||
  ( ! isset($_POST['descrizione']) || strlen($_POST['descrizione']) <= 0 )       ||

  ( ! isset($_POST['categorie'])   || count( (array) $_POST['categorie']) <= 0 ) ||
  ( ! isset($_POST['artisti'])     || count( (array) $_POST['artisti'])   <= 0 )
){
  send_json_error('Qualcosa è andato storto');
  die();
}

$titolo  = $_POST['titolo'];
$prov    = $_POST['provincia'];
$data    = $_POST['data'];
$desc    = $_POST['descrizione'];
$cats    = $_POST['categorie'];
$artisti = $_POST['artisti'];

require_once "connessioneDB.php";

$stmt = $connessione->prepare("SELECT * FROM evento WHERE titolo = ? AND dataEvento = ?");
$result = NULL;
if(
  $stmt === FALSE ||
  $stmt->bind_param("ss", $titolo, $data) === FALSE ||
  $stmt->execute() === FALSE ||
  ($result = $stmt->get_result()) === FALSE
){
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}
  
if ($result->num_rows >= 1) {
  send_json_error('Evento già inserito');
  close_conn_and_die();
}

$stmt = $connessione->prepare("INSERT INTO evento (dataEvento, titolo, descrizione, provinciaEvento, nicknameMembro) VALUES (?, ?, ?, ?, ?)");
if(
  $stmt === FALSE ||
  $stmt->bind_param("sssss", $data, $titolo, $desc, $prov, $nomeUtente) === FALSE ||
  $stmt->execute() === FALSE
){
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}
$id = $connessione->insert_id;

$stmt = $connessione->prepare("INSERT INTO categoriaEvento (IDEvento, nomeCategoria) VALUES (?, ?)");
$success = TRUE;
if(
  $stmt === FALSE ||
  $stmt->bind_param("is", $id, $categoria) === FALSE
){
  $success = FALSE;
}
if($success){
  foreach($cats as $categoria){
    $categoria = htmlspecialchars($categoria);
    $success = $stmt->execute();
    if( ! $success ){ break; }
  }
}

if(! $success){
  $stmt = $connessione->prepare("DELETE FROM evento WHERE IDEvento = ?");
  /*Uso il short circuiting al posto di 3 if*/
  $stmt === FALSE ||
  $stmt->bind_param("i", $id) === FALSE ||
  $sql->execute() === FALSE;
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}

$stmt = $connessione->prepare("INSERT INTO partecipazioneArtista (IDEvento, nomeArte) VALUES (?, ?)");
if(
  $stmt === FALSE ||
  $stmt->bind_param("is", $id, $artista) === FALSE
){
  $success = false;
}

if($success){
  foreach($artisti as $artista){
    $stmt->bind_param("is", $id, $artista);
    $artista = htmlspecialchars(substr($artista, 0, 32));
    $success = $stmt->execute();
    if( ! $success ){ break; }
  }
}

if( ! $success ){
  $stmt = $connessione->prepare("DELETE FROM evento WHERE IDEvento = ?");
  /*Uso il short circuiting al posto di 3 if*/
  $stmt === FALSE ||
  $stmt->bind_param("i", $id) === FALSE ||
  $sql->execute() === FALSE;

  $stmt = $connessione->prepare("DELETE FROM categoriaEvento WHERE IDEvento = ?");
  /*Uso il short circuiting al posto di 3 if*/
  $stmt === FALSE ||
  $stmt->bind_param("i", $id) === FALSE ||
  $sql->execute() === FALSE;

  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}
send_json_success(['added' => true]);
close_conn_and_die();
?>
