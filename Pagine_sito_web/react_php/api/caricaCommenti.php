<?php

require_once "util.php";
maybe_start_session();
if( ! ( isset($_POST['id']) && strlen($_POST['id']) > 0 && is_numeric($_POST['id'])) ){
    send_json_error('Qualcosa è andato storto');
    die();
}
$id = $_POST['id'];
$pagina = isset($_POST['pagina']) && strlen($_POST['pagina']) > 0 && is_numeric($_POST['pagina']) ? $_POST['pagina'] : 0;
$PAGE_LEN = comments_page_len();
$offset = $pagina * $PAGE_LEN;
$PAGE_LEN_TEST = $PAGE_LEN + 1;

require_once "connessioneDB.php";

$stmt = $connessione->prepare(
    "SELECT ID, dataCommento, commento, voto, nicknameUtente
     FROM post WHERE IDEvento = ?
     ORDER BY dataCommento DESC
     LIMIT ?, ?"
  );

if(
  $stmt === FALSE ||
  $stmt->bind_param("iii", $id, $offset, $PAGE_LEN_TEST) === FALSE ||
  $stmt->execute() === FALSE ||
  ($result = $stmt->get_result()) === FALSE
){
  send_json_error('Qualcosa è andato storto');
  close_conn_and_die();
}

$commenti = $result->fetch_all(MYSQLI_ASSOC);
$altri_commenti = false;
if(isset($commenti[$PAGE_LEN])){
  $altri_commenti = true;
  unset($commenti[$PAGE_LEN]);
}
send_json_success([
	  'commenti'       => $commenti,
	  'altri_commenti' => $altri_commenti,
  ]
);
close_conn_and_die();
?>
