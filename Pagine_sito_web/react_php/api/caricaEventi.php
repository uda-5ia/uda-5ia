<?php
/*
TODO
vedi commento sotto
*/

require_once "util.php";
maybe_start_session();

$PAGE_LEN = 5;

$provincia        = (isset($_POST["provincia"])  &&	strlen($_POST['provincia'])  > 0) ? $_POST["provincia"]  : "";

$cerca_in_regione = (isset($_POST["regione"])    &&	strlen($_POST['regione'])    > 0) && (strlen($provincia) > 0);

$categorie        = (
                      isset($_POST["categorie"])      &&	
                      (
                        is_array($_POST['categorie']) &&
                        count($_POST['categorie'])  > 0 &&
                        strlen($_POST['categorie'][0]) > 0
                      )
                    ) ?  $_POST["categorie"] : [];

//(isset($_POST["categorie"])  &&	strlen($_POST['categorie'])  > 0) ? $_POST["categorie"]  : [];

$artisti          = (
                      isset($_POST["artisti"])      &&	
                      (
                        is_array($_POST['artisti']) &&
                        count($_POST['artisti'])  > 0 &&
                        strlen($_POST['artisti'][0]) > 0
                      )
                    ) ?  $_POST["artisti"] : [];
                    
$dataInizio       = (isset($_POST["dataInizio"]) &&	strlen($_POST['dataInizio']) > 0) ? $_POST["dataInizio"] : "";
$dataFine         = (isset($_POST["dataFine"])   &&	strlen($_POST['dataFine'])   > 0) ? $_POST["dataFine"]   : "";
$pagina   	      = (isset($_POST["pagina"])   	 && strlen($_POST['pagina'])     > 0) ? $_POST["pagina"]     : "";
$ricerca 	        = (isset($_POST["ricerca"])  	 && strlen($_POST['ricerca'])    > 0) ? $_POST["ricerca"]    : "";

require_once "connessioneDB.php";

$categorie_join = false;

$artisti_join = false;
$artisti_da_cercare = 0;

$params_to_bind = "";
$params_to_bind_names = [];

$sql_regione = "";
$sql_provincia = "";
$sql_categorie = "";
$sql_artisti = "";
$sql_data = "";
$sql_ricerca = "";

$order = 'DESC';
$now = date('Y-m-d H:i:s');

$offset = 0;

$regione = "";
if($cerca_in_regione){
  $result = NULL;
  $stmt = $connessione->prepare(
    "SELECT regione FROM provincePerRegione WHERE provincia = ? LIMIT 1"
  );
  $stmt === FALSE ||
  $stmt->bind_param('s', $provincia) === FALSE ||
  $stmt->execute() == FALSE ||
  ($result = $stmt->get_result()) === FALSE;
  if($result->num_rows > 0){
    $result = $result->fetch_array();
    $regione = $result[0];
  }
}
if(strlen($regione) > 0){
	$sql_regione = "evento.provinciaEvento IN (" .
		"SELECT provincia
		FROM provincePerRegione
		WHERE regione = ?
	)";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$regione;
}elseif(strlen($provincia) > 0){
	$sql_provincia = "evento.provinciaEvento = ?";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$provincia;
}

if(!empty($categorie) && is_array($categorie)){

	$sql_categorie = "categoriaEvento.nomeCategoria IN (";

	foreach ($categorie as $categoria) {
		$sql_categorie .= "?,  ";
		$params_to_bind .= "s";
		$params_to_bind_names[] = &$categorie[array_search($categoria, $categorie)];
		$categorie_join = true;
	}
	$sql_categorie = substr($sql_categorie, 0, -strlen('?, ')) . ')';
}

if(!empty($artisti) && is_array($artisti)){

	$sql_artisti = 'partecipazioneArtista.nomeArte IN (';

	foreach ($artisti as $artista) {
		$sql_artisti .= "?, ";
		$params_to_bind .= "s";
		$params_to_bind_names[] = &$artisti[array_search($artista, $artisti)];
		$artisti_join = true;
	}
	
	$artisti_da_cercare = count($artisti);

	$sql_artisti = substr($sql_artisti, 0, -strlen(', ')) . ')';
	//$sql_artisti = substr($sql_artisti, 0, -strlen(' OR '));
}

if(strlen($ricerca) > 0){
	$ricerca = "%$ricerca%";
	$sql_ricerca .= "evento.titolo like ? OR evento.descrizione like ? ";
	$params_to_bind .= "ss";
	$params_to_bind_names[] = &$ricerca;
	$params_to_bind_names[] = &$ricerca;
}

//Date
if(strlen($dataInizio) > 0 && strlen($dataFine) > 0 && $dataInizio !== $dataFine){
	$sql_data = "evento.dataEvento BETWEEN ? AND ?";
	$params_to_bind .= "ss";
	$params_to_bind_names[] = &$dataInizio;
	$params_to_bind_names[] = &$dataFine;
	$order = 'ASC';
}

if(strlen($dataInizio) > 0 && strlen($dataFine) <= 0){
	$sql_data = "evento.dataEvento >= ?";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$dataInizio;
	$order = 'ASC';
}

if(strlen($dataFine) > 0 && strlen($dataInizio) <= 0){
	$sql_data = "evento.dataEvento BETWEEN ? AND ?";
	$params_to_bind .= "ss";
	$params_to_bind_names[] = &$now;
	$params_to_bind_names[] = &$dataFine;
	$order = 'ASC';
}

if(strlen($dataInizio) <= 0 && strlen($dataFine) <= 0){
	$sql_data = "evento.dataEvento >= ?";
	$params_to_bind .= "s";
	$params_to_bind_names[] = &$now;
	$order = 'ASC';
}

//Limit
$limit = " LIMIT ?, ?";
$offset = (strlen($pagina) > 0 && is_numeric($pagina)) ? ($pagina * $PAGE_LEN) : 0;

$PAGE_LEN_PLUS_ONE = $PAGE_LEN + 1;

$params_to_bind .= 'ii';
$params_to_bind_names[] = &$offset;
$params_to_bind_names[] = &$PAGE_LEN_PLUS_ONE;

$sql_where_params =
(strlen($sql_regione) 	> 0 ? (" AND " . $sql_regione) : "") .
(strlen($sql_provincia) > 0 ? (" AND " . $sql_provincia) : "") .
(strlen($sql_categorie) > 0 ? (" AND " . $sql_categorie) : "") .
(strlen($sql_artisti)   > 0 ? (" AND " . $sql_artisti) : "") .
(strlen($sql_ricerca)   > 0 ? (" AND " . $sql_ricerca) : "") .
(strlen($sql_data)   	> 0 ? (" AND " . $sql_data) : "");

$sql_where_params = substr($sql_where_params, strlen(" AND "));

$to_join = "";
if($categorie_join){
	$to_join .= " INNER JOIN categoriaEvento ON (evento.ID = categoriaEvento.IDEvento)";
}
if($artisti_join){
	$to_join .= " INNER JOIN partecipazioneArtista ON (evento.ID = partecipazioneArtista.IDEvento)";
}

$result = NULL;
$select_from_evento = "SELECT DISTINCT evento.ID, evento.provinciaEvento, SUBSTRING(evento.descrizione, 1, 351) AS descrizione, evento.dataEvento, evento.titolo FROM evento";

$sql = "$select_from_evento $to_join" .
(strlen($sql_where_params) > 0 ? " WHERE $sql_where_params" : '') .
($artisti_join ? " GROUP BY partecipazioneArtista.IDEvento HAVING COUNT(partecipazioneArtista.nomeArte) >= $artisti_da_cercare" : '') .
" ORDER BY evento.dataEvento $order" .
$limit;

$stmt = $connessione->prepare($sql);
$stmt->bind_param($params_to_bind, ...$params_to_bind_names);

$result = $stmt->execute();

if($result === TRUE){
	$result = $stmt->get_result();
	$altri_eventi = $result->num_rows >= $PAGE_LEN;
	$eventi = [];
	while($row = $result->fetch_assoc()){
		$eventi[] = [
			'id'		      => htmlspecialchars($row['ID']),
			'dataEvento'  => htmlspecialchars( substr($row['dataEvento'], 0, -3)),
			'titolo' 	    => htmlspecialchars($row['titolo']),
			'provincia'   => htmlspecialchars($row['provinciaEvento']),
			'descrizione' => htmlspecialchars($row['descrizione']),
			'altri_eventi' => $altri_eventi,
		];
	}
	if($altri_eventi){
		unset($eventi[$PAGE_LEN]);
	}
	if(! empty($eventi) && count($eventi) > 0){
		send_json_success($eventi, false);
	}else{
	  send_json_success('Nessun evento corrisponde ai criteri di ricerca');
  }
} else {
	send_json_error('Qualcosa è andato storto');
}
close_conn_and_die();
?>
