<h1>Per usare il contenuto del context CostantiContext</h1>
<h3>Se hai una classe che estende un componente:</h3>
<h5>inserisci a inizio file:</h5>
<p>"import { CostantiContext } from "../../CostantiContext";"</p>

<h5>subito dopo la riga dove dichiari la classe:</h5>
<p>"static contextType = CostantiContext;"</p>

<h4>Da ora in poi puoi riferirti a "this.context" per le variabili condivise</h4>

<h3>se hai una funzione:</h3>
<h5>inserisci a inizio file:</h5>
<p>"import { CostantiContext } from "../../CostantiContext";"</p>
<h5>dentro al return (); inserisci:</h5>
<p>"<CostantiContext.Consumer>{(context) => {

//Qui il codice JS

return (

//Qui il codice HTML

)
}}</CostantiContext.Consumer>"</p>
