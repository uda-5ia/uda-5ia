File che contiene la lista di tutorial usati per creare il sito react

Proposte:
Gestione centralizzata stile: https://css-tricks.com/a-dark-mode-toggle-with-react-and-themeprovider/ - https://www.smashingmagazine.com/2020/04/dark-mode-react-apps-styled-components/(devo ancora finirlo)

Backend:
Login: https://www.digitalocean.com/community/tutorials/how-to-add-login-authentication-to-react-applications
Context: https://www.youtube.com/watch?v=6RhOzQciVwI&list=PL4cUxeGkcC9hNokByJilPg5g9m2APUePI - https://ui.dev/react-context/ - https://www.youtube.com/watch?v=PMEO0Vrq56o - https://stackoverflow.com/questions/34351804/how-to-declare-a-global-variable-in-react - https://reactjs.org/docs/context.html
Routes: https://www.youtube.com/watch?v=hjR-ZveXBpQ - 404 https://ultimatecourses.com/blog/react-router-not-found-component
Docs: https://it.reactjs.org/tutorial/tutorial.html - https://it.reactjs.org/docs/handling-events.html - https://it.reactjs.org/docs/state-and-lifecycle.html - https://it.reactjs.org/docs/components-and-props.html
Axios: https://www.digitalocean.com/community/tutorials/react-axios-react

Frontend:
