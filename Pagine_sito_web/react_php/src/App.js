import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { CostantiContext } from "./context/costantiContext";

//Pagine
import IndexPage from "./pagine/index";
import LoginPage from "./pagine/login";
import RegisterPage from "./pagine/registrati";
import InserisciEventoPage from "./pagine/inserisci";
import CaricaEventiPage from "./pagine/visualizzaEventi";
import EventoPage from "./pagine/evento";
import UtentePage from "./pagine/utente";
import PageNotFoundPage from "./pagine/404";

export class App extends Component {
  static contextType = CostantiContext;

  render() {
    const classe =
      this.context.theme === "dark"
        ? "dark flex flex-col min-h-screen"
        : "flex flex-col min-h-screen";
    return (
      <div className={classe}>
        <div className="App general-background general-testo flex flex-col min-h-screen">
          <Router>
            <Switch>
              <Route exact path="/">
                <IndexPage />
              </Route>
              <Route path="/index">
                <IndexPage />
              </Route>
              <Route path="/login">
                <LoginPage />
              </Route>
              <Route path="/registrati">
                <RegisterPage />
              </Route>
              <Route path="/inserisciEventi">
                <InserisciEventoPage />
              </Route>
              <Route path="/visualizzaEventi">
                <CaricaEventiPage />
              </Route>
              <Route path="/utente">
                <UtentePage />
              </Route>
              <Route path="/evento">
                <EventoPage />
              </Route>
              <Route>
                <PageNotFoundPage />
              </Route>
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}
