import React, { useState } from "react";
import axios from "axios";
import { CostantiContext } from "../../context/costantiContext";
import { Categories } from "../categories/categories";
import { Location } from "../location/location";

export class Register extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);

    this.state = {
      nome: "",
      cognome: "",
      email: "",
      username: "",
      password: "",
      categorie: "",
      provincia: "",
      errore: "",
      nomeError: false,
      cognomeError: false,
      emailError: false,
      usernameError: false,
      passError: false,
      categorieError: false,
      provinceError: false,
      nomeValidated: false,
      cognomeValidated: false,
      emailValidated: false,
      usernameValidated: false,
      passValidated: false,
      categorieValidated: false,
      provinceValidated: false
    };
  }

  setProvincia = loc => {
    this.setState({ provincia: loc });
  };

  setCategorie = cat => {
    this.setState({ categorie: cat });
  };

  setProvinceError = prov => {
    this.setState({ provinceError: prov });
  };

  setCategorieError = cat => {
    this.setState({ categorieError: cat });
  };

  setProvinceValidated = prov => {
    this.setState({ provinceValidated: prov });
  };

  setCategorieValidated = cat => {
    this.setState({ categorieValidated: cat });
  };

  getProvinceError = () => {
    return this.state.provinceError;
  };

  getCategorieError = () => {
    return this.state.categorieError;
  };

  getProvinceValidated = () => {
    return this.state.provinceValidated;
  };

  getCategorieValidated = () => {
    return this.state.categorieValidated;
  };

  checkEmail = email => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  checkNameLName = nameLName => {
    const re = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)\s?)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;
    return re.test(nameLName);
  };

  checkUserame = username => {
    const re = /^[a-zA-Z0-9\!\_\+\-\*\#\$\^\.]+$/;
    return re.test(username);
  };

  checkPassword = password => {
    const strong = /^(?=.*[A-Z])(?=.*[\-\_\<\>\!\@\#\$\%\^\&\*\.])(?=.*[0-9])(?=.*[a-z]).{8,64}$/;
    const medium = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,64}$/;
    return strong.test(password) || medium.test(password);
  };

  colorSet = (error, validated) => {
    var inputClass =
      "placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full text-black";
    inputClass += error == true ? " bg-red-300" : "";
    inputClass += validated == true ? " bg-green-300" : "";
    return inputClass;
  };

  render() {
    if (this.context.logged()) {
      window.location.replace("/");
      return <div></div>;
    }

    return (
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full md:w-4/12 px-4 pt-32 pb-32 ">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg box-background border-0 ">
              <div className="flex-auto px-4 md:px-10 py-10 pt-0">
                <form className="mt-6">
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mt-2 mb-2"
                      htmlFor="grid-password"
                    >
                      Nome
                    </label>
                    <input
                      type="text"
                      className={this.colorSet(
                        this.state.nomeError,
                        this.state.nomeValidated
                      )}
                      placeholder="Nome"
                      onBlur={e => {
                        if (this.checkNameLName(e.target.value)) {
                          this.setState({ nomeError: false });
                          this.setState({ nomeValidated: true });
                          this.setState({ nome: e.target.value });
                        } else {
                          this.setState({ nomeError: true });
                          this.setState({ nomeValidated: false });
                        }
                      }}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mt-2 mb-2"
                      htmlFor="grid-password"
                    >
                      Cognome
                    </label>
                    <input
                      type="text"
                      className={this.colorSet(
                        this.state.cognomeError,
                        this.state.cognomeValidated
                      )}
                      placeholder="Cognome"
                      onBlur={e => {
                        if (this.checkNameLName(e.target.value)) {
                          this.setState({ cognomeError: false });
                          this.setState({ cognomeValidated: true });
                          this.setState({ cognome: e.target.value });
                        } else {
                          this.setState({ cognomeError: true });
                          this.setState({ cognomeValidated: false });
                        }
                      }}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mt-2 mb-2"
                      htmlFor="grid-password"
                    >
                      Email
                    </label>
                    <input
                      type="email"
                      className={this.colorSet(
                        this.state.emailError,
                        this.state.emailValidated
                      )}
                      placeholder="Email"
                      autoComplete="email"
                      onBlur={e => {
                        if (this.checkEmail(e.target.value)) {
                          this.setState({ emailError: false });
                          this.setState({ emailValidated: true });
                          this.setState({ email: e.target.value });
                        } else {
                          this.setState({ emailError: true });
                          this.setState({ emailValidated: false });
                        }
                      }}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mt-2 mb-2"
                      htmlFor="grid-password"
                    >
                      Nome utente
                    </label>
                    <input
                      type="username"
                      className={this.colorSet(
                        this.state.usernameError,
                        this.state.usernameValidated
                      )}
                      placeholder="Nome utente"
                      autoComplete="username"
                      onBlur={e => {
                        if (this.checkUserame(e.target.value)) {
                          this.setState({ usernameError: false });
                          this.setState({ usernameValidated: true });
                          this.setState({ username: e.target.value });
                        } else {
                          this.setState({ usernameError: true });
                          this.setState({ usernameValidated: false });
                        }
                      }}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mt-2 mb-2"
                      htmlFor="grid-password"
                    >
                      Password
                    </label>
                    <input
                      type="password"
                      className={this.colorSet(
                        this.state.passwordError,
                        this.state.passwordValidated
                      )}
                      placeholder="Password"
                      autoComplete="current-password"
                      onChange={e => {
                        if (this.checkPassword(e.target.value)) {
                          this.setState({ passwordError: false });
                          this.setState({ passwordValidated: true });
                          this.setState({ password: e.target.value });
                        } else {
                          this.setState({ passwordError: true });
                          this.setState({ passwordValidated: false });
                        }
                      }}
                      maxLength="64"
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <div className="block uppercase text-xs font-bold mt-2 mb-2">
                      Categorie
                    </div>
                    <Categories
                      setCategorie={this.setCategorie}
                      getCategorieError={this.getCategorieError}
                      getCategorieValidated={this.getCategorieValidated}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <div className="block uppercase text-xs font-bold mt-2 mb-2">
                      Provincia
                    </div>
                    <Location
                      setProvincia={this.setProvincia}
                      getProvinceError={this.getProvinceError}
                      getProvinceValidated={this.getProvinceValidated}
                    />
                  </div>
                  <div className="text-center mt-6">
                    <button
                      className="box-button-background active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                      type="submit"
                      name="login"
                      value="login"
                      onClick={e => {
                        e.preventDefault();
                        if (
                          this.state.nome != "" &&
                          this.state.cognome != "" &&
                          this.state.email != "" &&
                          this.state.username != "" &&
                          this.state.password != "" &&
                          this.state.categorie != "" &&
                          this.state.categorie != [] &&
                          this.state.provincia != "" &&
                          this.state.nomeValidated == true &&
                          this.state.cognomeValidated == true &&
                          this.state.emailValidated == true &&
                          this.state.usernameValidated == true &&
                          this.state.passwordValidated == true &&
                          this.state.nomeError == false &&
                          this.state.cognomeError == false &&
                          this.state.emailError == false &&
                          this.state.usernameError == false &&
                          this.state.passwordError == false
                        ) {
                          const transport = axios.create({
                            withCredentials: true
                          });

                          transport
                            .post(
                              this.context.phpPath + "registrazione.php",
                              {
                                nome: this.state.nome,
                                cognome: this.state.cognome,
                                email: this.state.email,
                                username: this.state.username,
                                password: this.state.password,
                                categorie: this.state.categorie,
                                provincia: this.state.provincia,
                                sessionId: this.context.getSessionId()
                              },
                              {
                                headers: { "content-type": "application/json" }
                              }
                            )
                            .then(response => {
                              if (response.data.success == true) {
                                window.location.replace("/");
                              } else {
                                this.setState({ nomeError: false });
                                this.setState({ cognomeError: false });
                                this.setState({ emailError: false });
                                this.setState({ userError: false });
                                this.setState({ passwordError: false });
                                this.setState({ nomeValidated: false });
                                this.setState({ cognomeValidated: false });
                                this.setState({ emailValidated: false });
                                this.setState({ userValidated: false });
                                this.setState({ passwordValidated: false });
                                if (
                                  response.data.data.msg ==
                                  "Qualcosa è andato storto"
                                ) {
                                  this.setState({
                                    errore: "Qualcosa è andato storto"
                                  });
                                } else if (
                                  response.data.data.msg ==
                                  "C'e gia un account con quell'email."
                                ) {
                                  this.setState({ emailError: true });
                                  this.setState({
                                    errore:
                                      "C'e gia un account con quell'email."
                                  });
                                } else if (
                                  response.data.data.msg ==
                                  "C'è già un account con quel nome utente."
                                ) {
                                  this.setState({ userError: true });
                                  this.setState({
                                    errore:
                                      "C'è già un account con quel nome utente."
                                  });
                                } else if (
                                  response.data.data.msg ==
                                  "Qualcosa è andato storto nell'inserimento degli interessi!"
                                ) {
                                  this.setState({
                                    errore:
                                      "Qualcosa è andato storto nell'inserimento degli interessi!"
                                  });
                                } else if (
                                  response.data.data.msg ==
                                  "Qualcosa è andato storto nell'inserimento dell'utente nel DB!"
                                ) {
                                  this.setState({
                                    errore:
                                      "Qualcosa è andato storto nell'inserimento dell'utente nel DB!"
                                  });
                                }
                              }
                            });
                        }
                      }}
                    >
                      Register
                    </button>
                  </div>
                  <div
                    className={
                      this.state.errore == ""
                        ? "hidden"
                        : "text-md text-red-600 flex justify-center "
                    }
                  >
                    {this.state.errore}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
