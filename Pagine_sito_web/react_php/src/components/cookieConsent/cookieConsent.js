import React from "react";
import axios from "axios";
import { CostantiContext } from "../../context/costantiContext";
import CookieConsent, {
  Cookies,
  getCookieConsentValue,
} from "react-cookie-consent";

console.log(getCookieConsentValue());

export class CookieConsentBanner extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <CookieConsent
        location="bottom"
        cookieName="myAwesomeCookieName3"
        expires={999}
        overlay
      >
        This website uses cookies to enhance the user experience.
      </CookieConsent>
    );
  }
}

//export default Login;
