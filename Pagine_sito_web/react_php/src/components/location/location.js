import React, { useState, Component } from "react";
import Select, { components } from "react-select";
import axios from "axios";
import { CostantiContext } from "../../context/costantiContext";

export class Location extends Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);
    this.state = {
      list: "",
      isLoading: true,
      defaultValues: ""
    };
  }

  componentDidMount() {
    const transport = axios.create({
      withCredentials: true
    });

    transport
      .post(
        this.context.phpPath + "caricaProvince.php",
        {
          sessionId: this.context.getSessionId()
        },
        {
          headers: { "content-type": "application/json" }
        }
      )
      .then(response => {
        if (response.data.success == true) {
          this.setState({ list: response.data.data[0] });
          this.setState({ isLoading: false });
        }
      });
  }

  defaultValue = () => {
    let r = [];
    while (this.state.list != "") {
      if (this.props.defaultValues != null) {
        let def = this.props.defaultValues;
        let lista = this.state.list;
        let list = this.state.list.options;
        list.forEach((item1, i) => {
          item1.options.forEach((item2, o) => {
            if (item2.value == def) {
              r.push(this.state.list["options"][i]["options"][o]);
            }
          });
        });
      }
      break;
    }
    return r;
  };

  render() {
    const { isLoading, list } = this.state;
    const groupStyles = {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between"
    };
    const groupBadgeStyles = {
      backgroundColor: "#EBECF0",
      borderRadius: "2em",
      color: "#000",
      display: "inline-block",
      fontSize: 12,
      fontWeight: "normal",
      lineHeight: "1",
      minWidth: 1,
      padding: "0.16666666666667em 0.5em",
      textAlign: "center"
    };
    const formatGroupLabel = data => (
      <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
      </div>
    );
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: this.props.getProvinceError()
          ? "#FCA5A5"
          : this.props.getProvinceValidated()
          ? "#B6D53C"
          : "#FFFFFF"
      }),
      indicatorSeparator: (base, state) => ({
        ...base,
        backgroundColor: "#000000"
      }),
      dropdownIndicator: (base, state) => ({
        ...base,
        color: "#000000"
      })
    };
    const Placeholder = props => {
      return <components.Placeholder {...props} />;
    };
    return (
      <div>
        {this.state.list != "" ? (
          <Select
            components={{ Placeholder }}
            styles={customStyles}
            placeholder={"Province"}
            isLoading={isLoading}
            options={list.options}
            isClearable
            onChange={option => {
              if (option != null) {
                this.props.setProvincia(option.value);
              } else {
                this.props.setProvincia("");
              }
            }}
            className="text-black"
            formatGroupLabel={formatGroupLabel}
            defaultValue={this.defaultValue()}
          />
        ) : (
          <div></div>
        )}
      </div>
    );
    return <div></div>;
  }
}
