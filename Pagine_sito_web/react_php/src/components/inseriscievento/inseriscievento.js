import React, { useState } from "react";
import axios from "axios";
import DatePicker from "react-datepicker";
import { CostantiContext } from "../../context/costantiContext";
import { Categories } from "../categories/categories";
import { Location } from "../location/location";
import { Artisti } from "../artisti/artisti";

export class InserisciEvento extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);

    this.state = {
      titolo: "",
      categorie: "",
      provincia: "",
      artisti: "",
      data: "",
      descrizione: "",
      titoloValidated: false,
      provinceValidated: false,
      artistiValidated: false,
      categorieValidated: false,
      titoloError: false,
      provinceError: false,
      artistiError: false,
      categorieError: false,
      errore: "",
      success: ""
    };
  }

  setProvincia = loc => {
    this.setState({ provincia: loc });
  };

  setCategorie = cat => {
    this.setState({ categorie: cat });
  };

  setArtisti = art => {
    this.setState({ artisti: art });
  };

  setData = d => {
    const date =
      d.getFullYear() +
      "-" +
      ("0" + (d.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + d.getDate()).slice(-2) +
      " " +
      ("0" + d.getHours()).slice(-2) +
      ":" +
      ("0" + d.getMinutes()).slice(-2) +
      ":00";
    this.setState({ data: date });
  };

  setProvinceError = prov => {
    this.setState({ provinceError: prov });
  };

  setArtistiError = art => {
    this.setState({ artistiError: art });
  };

  setCategorieError = cat => {
    this.setState({ categorieError: cat });
  };

  setProvinceValidated = prov => {
    this.setState({ provinceValidated: prov });
  };

  setArtistiValidated = art => {
    this.setState({ artistiValidated: art });
  };

  setCategorieValidated = cat => {
    this.setState({ categorieValidated: cat });
  };

  getProvinceError = () => {
    return this.state.provinceError;
  };

  getArtistiError = () => {
    return this.state.artistiError;
  };

  getCategorieError = () => {
    return this.state.categorieError;
  };

  getProvinceValidated = () => {
    return this.state.provinceValidated;
  };

  getArtistiValidated = () => {
    return this.state.artistiValidated;
  };

  getCategorieValidated = () => {
    return this.state.categorieValidated;
  };

  checkTitolo = titolo => {
    const re = /^[a-zA-Z0-9\s]+$/;
    return re.test(titolo);
  };

  colorSet = (error, validated) => {
    var inputClass =
      "placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full text-black";
    inputClass += error == true ? " bg-red-300" : "";
    inputClass += validated == true ? " bg-green-300" : "";
    return inputClass;
  };

  render() {
    if (!this.context.logged()) {
      window.location.replace("/");
      return <div></div>;
    }

    return (
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full md:w-4/12 px-4 pt-32">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg box-background border-0">
              <div className="flex-auto px-4 md:px-10 py-10 pt-0">
                <form className="mt-6">
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Titolo
                    </label>
                    <input
                      type="titolo"
                      className={this.colorSet(
                        this.state.titoloError,
                        this.state.titoloValidated
                      )}
                      placeholder="Titolo"
                      onBlur={e => {
                        if (this.checkTitolo(e.target.value)) {
                          this.setState({ titoloError: false });
                          this.setState({ titoloValidated: true });
                          this.setState({ titolo: e.target.value });
                        } else {
                          this.setState({ titoloError: true });
                          this.setState({ titoloValidated: false });
                        }
                      }}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <div className="block uppercase text-xs font-bold mt-2 mb-2">
                      Categorie
                    </div>
                    <Categories
                      setCategorie={this.setCategorie}
                      getCategorieError={this.getCategorieError}
                      getCategorieValidated={this.getCategorieValidated}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <div className="block uppercase text-xs font-bold mt-2 mb-2">
                      Provincia
                    </div>
                    <Location
                      setProvincia={this.setProvincia}
                      getProvinceError={this.getProvinceError}
                      getProvinceValidated={this.getProvinceValidated}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <div className="block uppercase text-xs font-bold mt-2 mb-2">
                      Artisti
                    </div>
                    <Artisti
                      setArtisti={this.setArtisti}
                      getArtistiError={this.getArtistiError}
                      getArtistiValidated={this.getArtistiValidated}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <div className="block uppercase text-xs font-bold mt-2 mb-2">
                      Data
                    </div>
                    <Data
                      setData={this.setData}
                      colorSet={this.colorSet(
                        this.state.dataError,
                        this.state.dataValidated
                      )}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Descrizione
                    </label>
                    <textarea
                      type="titolo"
                      className="px-3 text-black py-3 placeholder-gray-750 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                      placeholder="Descrizione"
                      onBlur={e => {
                        let des = e.target.value;
                        des = des.substr(0, 102400);
                        this.setState({ descrizione: des });
                      }}
                      maxLength="102400"
                    />
                  </div>
                  <div className="text-center mt-6">
                    <button
                      className="box-button-background active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                      type="submit"
                      onClick={e => {
                        e.preventDefault();

                        if (
                          this.state.titolo != "" &&
                          this.state.categorie != "" &&
                          this.state.categorie != [] &&
                          this.state.provincia != "" &&
                          this.state.artisti != "" &&
                          this.state.artisti != [] &&
                          this.state.data != "" &&
                          this.state.descrizione != "" &&
                          this.state.titoloValidated == true &&
                          this.state.titoloError == false
                        ) {
                          const transport = axios.create({
                            withCredentials: true
                          });

                          transport
                            .post(
                              this.context.phpPath + "inserisciEvento.php",
                              {
                                titolo: this.state.titolo,
                                categorie: this.state.categorie,
                                artisti: this.state.artisti,
                                provincia: this.state.provincia,
                                data: this.state.data,
                                descrizione: this.state.descrizione,
                                sessionId: this.context.getSessionId()
                              },
                              {
                                headers: { "content-type": "application/json" }
                              }
                            )
                            .then(response => {
                              if (response.data.success == true) {
                                this.setState({
                                  success: "Evento inserito con successo!"
                                });
                                this.setState({
                                  errore: ""
                                });
                              } else {
                                this.setState({ titoloValidated: false });
                                this.setState({ provinceValidated: false });
                                this.setState({ artistiValidated: false });
                                this.setState({ categorieValidated: false });
                                this.setState({ dataValidated: false });
                                this.setState({ descrizioneValidated: false });
                                this.setState({ titoloError: false });
                                this.setState({ provinceError: false });
                                this.setState({ artistiError: false });
                                this.setState({ categorieError: false });
                                this.setState({ dataError: false });
                                this.setState({ descrizioneError: false });
                                if (
                                  response.data.data.msg == "Utente non loggato"
                                ) {
                                  this.context.remove();
                                  window.location.replace("/login/");
                                } else if (
                                  response.data.data.msg ==
                                  "Qualcosa è andato storto"
                                ) {
                                  this.setState({ emailError: true });
                                  this.setState({
                                    errore: "Qualcosa è andato storto."
                                  });
                                  this.setState({
                                    success: ""
                                  });
                                } else if (
                                  response.data.data.msg ==
                                  "Evento già inserito"
                                ) {
                                  this.setState({ userError: true });
                                  this.setState({
                                    errore: "Evento già inserito"
                                  });
                                  this.setState({
                                    success: ""
                                  });
                                }
                              }
                            });
                        }
                      }}
                    >
                      Inserisci
                    </button>
                  </div>
                  <div
                    className={
                      this.state.errore == ""
                        ? "hidden"
                        : "text-md text-red-600 flex justify-center "
                    }
                  >
                    {this.state.errore}
                  </div>
                  <div
                    className={
                      this.state.success == ""
                        ? "hidden"
                        : "text-md text-green-300 flex justify-center "
                    }
                  >
                    {this.state.success}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function Data(props) {
  const [startDate, setStartDate] = useState(new Date());

  return (
    <DatePicker
      className={props.colorSet}
      selected={startDate}
      onChange={date => {
        setStartDate(date);
        props.setData(date);
      }}
      dateFormat="dd-MM-yyyy HH:mm"
      minDate={new Date()}
      showTimeInput
      closeOnScroll={true}
    />
  );
}
