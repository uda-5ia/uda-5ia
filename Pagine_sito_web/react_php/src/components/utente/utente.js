import React, { useState } from "react";
import axios from "axios";
import { CostantiContext } from "../../context/costantiContext";
import { Categories } from "../categories/categories";
import { Location } from "../location/location";

export class Utente extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);

    this.state = {
      username: "",
      email: "",
      nome: "",
      cognome: "",
      categorie: "",
      provincia: "",
      errore: "",
      success: "",
      emailSelezionata: "",
      categorieSelezionate: "",
      provinciaSelezionata: "",
      emailError: false,
      provinceError: false,
      categorieError: false,
      emailValidated: false,
      provinceValidated: false,
      categorieValidated: false,
      modificato: false
    };
  }

  componentDidMount() {
    const transport = axios.create({
      withCredentials: true
    });

    transport
      .post(
        this.context.phpPath + "caricaUtente.php",
        {
          sessionId: this.context.getSessionId()
        },
        {
          headers: { "content-type": "application/json" }
        }
      )
      .then(response => {
        if (response.data.success == true) {
          let dati = response.data.data[0];
          this.setState({ username: dati.user_nickname });
          this.setState({ emailSelezionata: dati.email });
          this.setState({ nome: dati.nome });
          this.setState({ cognome: dati.cognome });
          this.setState({ categorieSelezionate: dati.categorie });
          this.setState({ provinciaSelezionata: dati.provincia });
        } else {
          if (response.data.data.msg == "Qualcosa è andato storto") {
            this.setState({
              errore: "Qualcosa è andato storto"
            });
          } else if (response.data.data.msg == "Utente non loggato") {
            this.context.remove();
            window.location.replace("/login/");
          }
        }
      });
  }

  setProvincia = loc => {
    this.setState({ provincia: loc });
    if (loc == "" || loc == this.state.provinciaSelezionata) {
      this.setState({ modificato: false });
    } else {
      this.setState({ modificato: true });
    }
  };

  setCategorie = cat => {
    this.setState({ categorie: cat });
    if (
      cat == "" ||
      cat == [] ||
      JSON.stringify(cat) == JSON.stringify(this.state.categorieSelezionate)
    ) {
      this.setState({ modificato: false });
    } else {
      this.setState({ modificato: true });
    }
  };

  setProvinceError = prov => {
    this.setState({ provinceError: prov });
  };

  setCategorieError = cat => {
    this.setState({ categorieError: cat });
  };

  setProvinceValidated = prov => {
    this.setState({ provinceValidated: prov });
  };

  setCategorieValidated = cat => {
    this.setState({ categorieValidated: cat });
  };

  getProvinceError = () => {
    return this.state.provinceError;
  };

  getCategorieError = () => {
    return this.state.categorieError;
  };

  getProvinceValidated = () => {
    return this.state.provinceValidated;
  };

  getCategorieValidated = () => {
    return this.state.categorieValidated;
  };

  checkEmail = email => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  colorSet = (error, validated) => {
    var inputClass =
      "placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full text-black";
    inputClass += error == true ? " bg-red-300" : "";
    inputClass += validated == true ? " bg-green-300" : "";
    return inputClass;
  };

  render() {
    if (!this.context.logged()) {
      window.location.replace("/");
      return <div></div>;
    }

    let classi =
      "box-button-background active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full";
    if (this.state.modificato == false) {
      classi += " hidden";
    }

    return (
      <div className="container mx-auto px-4 h-full pt-32 pb-32">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full md:w-4/12 px-4">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg box-background border-0">
              <div className="flex-auto px-4 md:px-10 py-10 pt-0">
                <form className="mt-6">
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Username
                    </label>
                    <input
                      className="text-gray-500 placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full text-black"
                      placeholder="Username"
                      defaultValue={this.state.username}
                      disabled
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Password
                    </label>
                    <input
                      type="password"
                      className="text-gray-500 placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full text-black"
                      placeholder="Password"
                      value="········"
                      disabled
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mt-2 mb-2"
                      htmlFor="grid-password"
                    >
                      Email
                    </label>
                    <input
                      type="email"
                      className={this.colorSet(
                        this.state.emailError,
                        this.state.emailValidated
                      )}
                      placeholder="Email"
                      autoComplete="email"
                      defaultValue={this.state.emailSelezionata}
                      onBlur={e => {
                        if (this.checkEmail(e.target.value)) {
                          this.setState({ emailError: false });
                          this.setState({ emailValidated: true });
                          this.setState({ email: e.target.value });
                        } else {
                          this.setState({ emailError: true });
                          this.setState({ emailValidated: false });
                        }

                        if (
                          e.target.value == "" ||
                          e.target.value == this.state.emailSelezionata
                        ) {
                          this.setState({ modificato: false });
                        } else {
                          this.setState({ modificato: true });
                        }
                      }}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mt-2 mb-2"
                      htmlFor="grid-password"
                    >
                      Nome
                    </label>
                    <input
                      type="text"
                      className="text-gray-500 placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full text-black"
                      placeholder="Nome"
                      defaultValue={this.state.nome}
                      disabled
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mt-2 mb-2"
                      htmlFor="grid-password"
                    >
                      Cognome
                    </label>
                    <input
                      type="text"
                      className="text-gray-500 placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full text-black"
                      placeholder="Cognome"
                      defaultValue={this.state.cognome}
                      disabled
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <div className="block uppercase text-xs font-bold mt-2 mb-2">
                      Categorie
                    </div>
                    {this.state.categorieSelezionate == "" ? (
                      <div></div>
                    ) : (
                      <Categories
                        setCategorie={this.setCategorie}
                        getCategorieError={this.getCategorieError}
                        getCategorieValidated={this.getCategorieValidated}
                        defaultValues={this.state.categorieSelezionate}
                      />
                    )}
                  </div>
                  <div className="relative w-full mb-3">
                    <div className="block uppercase text-xs font-bold mt-2 mb-2">
                      Provincia
                    </div>
                    {this.state.categorieSelezionate == "" ? (
                      <div></div>
                    ) : (
                      <Location
                        setProvincia={this.setProvincia}
                        getProvinceError={this.getProvinceError}
                        getProvinceValidated={this.getProvinceValidated}
                        defaultValues={this.state.provinciaSelezionata}
                      />
                    )}
                  </div>
                  <div className="text-center mt-6">
                    <button
                      className={classi}
                      type="submit"
                      name="login"
                      value="login"
                      onClick={e => {
                        e.preventDefault();

                        const transport = axios.create({
                          withCredentials: true
                        });

                        transport
                          .post(
                            this.context.phpPath + "aggiornaUtente.php",
                            {
                              email: this.state.email,
                              categorie: this.state.categorie,
                              provincia: this.state.provincia,
                              sessionId: this.context.getSessionId()
                            },
                            {
                              headers: { "content-type": "application/json" }
                            }
                          )
                          .then(response => {
                            console.log(response);
                            if (response.data.success == true) {
                              if (response.data.data[0].modificato == true) {
                                this.setState({ success: "Modificato!" });
                              } else {
                                this.setState({
                                  error: "Impossibile modificare"
                                });
                              }
                            } else if (
                              response.data.data.msg == "Utente non loggato"
                            ) {
                              this.context.remove();
                              window.location.replace("/login/");
                            } else {
                              this.setState({
                                error: "Qualcosa è andato storto"
                              });
                            }
                          });
                      }}
                    >
                      Modifica
                    </button>
                  </div>
                  <div
                    className={
                      this.state.errore == ""
                        ? "hidden"
                        : "text-md text-red-600 flex justify-center "
                    }
                  >
                    {this.state.errore}
                  </div>
                  <div
                    className={
                      this.state.success == ""
                        ? "hidden"
                        : "text-md text-green-300 flex justify-center "
                    }
                  >
                    {this.state.success}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
