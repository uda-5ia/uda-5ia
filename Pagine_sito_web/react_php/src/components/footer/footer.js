import React from "react";
import axios from "axios";
import { CostantiContext } from "../../context/costantiContext";

export class Footer extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const a = "text-md hover:text-blue-400 hover:underline";
    return (
      <footer className="border-t-2 footer-border items-center footer footer-background relative pt-1 ">
        <div className="container mx-auto px-6">
          <div className="md:flex md:mt-8">
            <div className="mt-8 md:mt-0 md:w-full md:px-8 flex flex-col md:flex-row justify-between footer-text">
              <div className="flex flex-col items-center">
                <span className="font-bold uppercase mb-2">Chi siamo</span>
                <span className="my-2">
                  <a href="https://gitlab.com/Lorenzobaldo02" className={a}>
                    Baldo Lorenzo
                  </a>
                </span>
                <span className="my-2">
                  <a href="https://gitlab.com/_Zaizen_" className={a}>
                    Canello Leonardo
                  </a>
                </span>
                <span className="my-2">
                  <a href="https://gitlab.com/denislucietto2" className={a}>
                    Lucietto Denis
                  </a>
                </span>
                <span className="my-2">
                  <a href="https://gitlab.com/FilippoRizzolo" className={a}>
                    Rizzolo Filippo
                  </a>
                </span>
                <span className="my-2">
                  <a href="https://gitlab.com/gabrieletrevisan02" className={a}>
                    Trevisan Gabriele
                  </a>
                </span>
              </div>
              <div className="flex flex-col items-center">
                <span className="font-bold uppercase mt-4 md:mt-0 mb-2">
                  Dove trovarci
                </span>
                <span className="my-2">
                  <a href="https://gitlab.com/uda-5ia" className={a}>
                    GitLab
                  </a>
                </span>
                <span className="my-2">
                  <a href="https://www.itiseveripadova.edu.it/" className={a}>
                    ITI Severi Padova
                  </a>
                </span>
              </div>
              <div className="flex flex-col items-center">
                <span className="font-bold uppercase mt-4 md:mt-0 mb-2">
                  Tecnologie usate
                </span>
                <span className="my-2">
                  <a href="https://reactjs.org/" className={a}>
                    React
                  </a>
                </span>
                <span className="my-2">
                  <a href="https://tailwindcss.com/" className={a}>
                    Tailwindcss
                  </a>
                </span>
                <span className="my-2">
                  <a href="https://www.php.net/" className={a}>
                    PHP
                  </a>
                </span>
                <span className="my-2">
                  <a href="https://www.mysql.com/" className={a}>
                    MySQL
                  </a>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="w-100 border-t-2 footer-border flex flex-col items-center">
          <div className="md:w-2/3 text-center py-6">
            <p className="text-sm footer-text font-bold mb-2">
              © 2021 by Gruppo Urano
            </p>
          </div>
        </div>
      </footer>
    );
  }
}

//export default Login;
