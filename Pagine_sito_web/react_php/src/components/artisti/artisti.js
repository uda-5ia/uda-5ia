import React, { useState, Component } from "react";
import Select, { components } from "react-select";
import axios from "axios";
import { CostantiContext } from "../../context/costantiContext";

export class Artisti extends Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);
    this.state = {
      list: "",
      isLoading: true
    };
  }

  componentDidMount() {
    const transport = axios.create({
      withCredentials: true
    });

    transport
      .post(
        this.context.phpPath + "caricaArtisti.php",
        {
          sessionId: this.context.getSessionId()
        },
        {
          headers: { "content-type": "application/json" }
        }
      )
      .then(response => {
        if (response.data.success == true) {
          this.setState({ list: response.data.data[0] });
          this.setState({ isLoading: false });
        }
      });
  }

  render() {
    const { isLoading, list } = this.state;
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: this.props.getArtistiError()
          ? "#FCA5A5"
          : this.props.getArtistiValidated()
          ? "#B6D53C"
          : "#FFFFFF"
      }),
      indicatorSeparator: (base, state) => ({
        ...base,
        backgroundColor: "#000000"
      }),
      dropdownIndicator: (base, state) => ({
        ...base,
        color: "#000000"
      })
    };
    const Placeholder = props => {
      return <components.Placeholder {...props} />;
    };

    return (
      <Select
        components={{ Placeholder }}
        styles={customStyles}
        placeholder={"Artisti"}
        isLoading={isLoading}
        options={list.options}
        isMulti
        className="text-black"
        onChange={option => {
          let ris = [];
          option.forEach((item, i) => {
            ris.push(item.value);
          });
          this.props.setArtisti(ris);
        }}
      />
    );
    return <div></div>;
  }
}
