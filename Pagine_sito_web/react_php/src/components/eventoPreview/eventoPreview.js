import React, { useState, Component } from "react";
import Select, { components } from "react-select";
import axios from "axios";
import { Link } from "react-router-dom";
import { CostantiContext } from "../../context/costantiContext";

export class EventoPreview extends Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let descrizione = this.props.descrizione;
    let lungMax = 350;
    if (descrizione.length > lungMax) {
      let lastSpace = 0,
        i = 0,
        spaces = descrizione.match(/([\s]+)/g).length;
      for (i = 0; i < spaces; i++) {
        let des = descrizione.indexOf(" ", lastSpace + i);
        if (des >= lungMax) {
          lastSpace = descrizione.indexOf(" ", lastSpace - 1);

          break;
        }

        lastSpace = des;
      }
      descrizione = descrizione.substr(0, lastSpace + 1);
      descrizione += "...";
    }
    let elimina =
      this.context.userPrivileges == "A" ||
      this.context.userPrivileges == "a" ? (
        <div className="md:flex-col">
          <div className="justify-center mt-8 flex md:pt-2 md:pb-2 md:flex-shrink-0">
            <div className="md:flex-col  rounded-md shadow">
              <button
                onClick={() => {
                  const transport = axios.create({
                    withCredentials: true
                  });

                  transport
                    .post(
                      this.context.phpPath + "eliminaEvento.php",
                      {
                        id: this.props.id,
                        sessionId: this.context.getSessionId()
                      },
                      {
                        headers: { "content-type": "application/json" }
                      }
                    )
                    .then(response => {
                      if (response.data.success == true) {
                        this.props.cerca();
                      } else {
                        if (
                          response.data.data.msg == "Utente non loggato" ||
                          response.data.data.msg == "Qualcosa è andato storto"
                        ) {
                          this.context.remove();
                          window.location.replace("/");
                        }
                      }
                    });
                }}
                className="flex-col items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md general-testo box-button-background"
              >
                Elimina
              </button>
            </div>
          </div>
        </div>
      ) : (
        <div></div>
      );

    let evento = "/evento/" + this.props.id;

    return (
      <div className="pt-5 pb-5">
        <div className="px-8 w-full">
          <div className="px-2 md:px-8 h-full border-b-2 general-border rounded-lg box-background w-full pt-2 pb-2 md:pt-4 md:pt-4">
            <div className="mx-auto py-4 px-2 md:py-4 md:px-4 md:flex md:items-center md:justify-between">
              <div className="md:float-left md:w-full tracking-tight text-center md:px-8 md:text-left general-testo md:text-4xl">
                <span className="md:flex md:items-center md:justify-right">
                  <Link to={evento}>
                    <p className="text-3xl hover:underline font-extrabold">
                      {this.props.titolo}
                    </p>
                  </Link>
                  <div className="md:flex md:ml-auto md:mr">
                    <span className="block text-xl mr-5">
                      A {this.props.provincia}
                    </span>
                    <span className="block text-xl">Il {this.props.data}</span>
                  </div>
                </span>
                <span className="text-xl font-bold general-testo">
                  {descrizione}
                </span>
              </div>
              {elimina}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
