import React, { useState, Component } from "react";
import Select, { components } from "react-select";
import axios from "axios";
import { CostantiContext } from "../../context/costantiContext";

export class Commento extends Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let elimina =
      this.context.userPrivileges == "A" ||
      this.context.userPrivileges == "a" ||
      this.context.userPrivileges == "M" ||
      this.context.userPrivileges == "m" ? (
        <div className="md:flex-col">
          <div className="justify-center mt-8 flex md:pt-2 md:pb-2 md:flex-shrink-0">
            <div className="md:flex-col  rounded-md shadow">
              <button
                onClick={() => {
                  const transport = axios.create({
                    withCredentials: true
                  });

                  transport
                    .post(
                      this.context.phpPath + "eliminaCommento.php",
                      {
                        id: this.props.id,
                        sessionId: this.context.getSessionId()
                      },
                      {
                        headers: { "content-type": "application/json" }
                      }
                    )
                    .then(response => {
                      if (response.data.success == true) {
                        this.props.ricarica();
                      } else {
                        if (response.data.data.msg == "Utente non loggato") {
                          this.context.remove();
                          window.location.replace("/login/");
                        } else if (
                          response.data.data.msg == "Permesso negato"
                        ) {
                          this.context.remove();
                          window.location.replace("/login/");
                        }
                      }
                    });
                }}
                className="flex-col items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md general-button-testo box-button-background"
              >
                Elimina
              </button>
            </div>
          </div>
        </div>
      ) : (
        <div></div>
      );

    const chiave = 999 + this.props.id;

    return (
      <div className="pt-5 pb-5 md:pr-20">
        <div className="px-8 w-full">
          <div className="px-2 md:px-2 h-full border-b-2 rounded-lg box-background w-full pt-2 pb-2 md:pt-4 md:pb-4">
            <div className="mx-auto py-2 px-2 md:py-2 md:px-2 md:flex md:items-center md:justify-between">
              <div className="md:float-left md:w-full tracking-tight text-center md:px-2 md:text-left general-testo md:text-4xl">
                <span className="md:flex md:items-center md:justify-right">
                  <p className="text-3xl font-extrabold">{this.props.autore}</p>
                  <div className="md:flex md:ml-auto md:mr">
                    <span className="block text-xl md:pr-5">
                      Data: {this.props.data}
                    </span>
                    <div className="m-auto items-center justify-between">
                      <Stars key={chiave} voto={this.props.voto} />
                    </div>
                  </div>
                </span>
                <span className="text-xl font-bold general-testo">
                  {this.props.commento}
                </span>
              </div>
              {elimina}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function Stars(props) {
  let star = (
    <svg
      className="block h-8 w-8"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
    >
      <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
    </svg>
  );

  let finalStar = [];
  for (let i = 0; i < props.voto; i++) {
    finalStar.push(star);
  }
  return <div className="flex justify-center">{finalStar}</div>;
}

//autore, voto, commento e data
