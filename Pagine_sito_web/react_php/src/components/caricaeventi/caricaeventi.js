import React, { useState } from "react";
import axios from "axios";
import { CostantiContext } from "../../context/costantiContext";
import { Categories } from "../categories/categories";
import { Location } from "../location/location";
import { Artisti } from "../artisti/artisti";
import { EventoPreview } from "../eventoPreview/eventoPreview";
import DatePicker from "react-datepicker";

export class CaricaEventi extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);

    this.state = {
      cerca: "",
      categorie: "",
      provincia: "",
      artisti: "",
      dataInizio: "",
      dataFine: "",
      eventi: [],
      vediCheck: false,
      toggleRegione: false,
      errore: "",
      pagina: 0,
      altriEventi: true
    };
  }

  componentDidMount() {
    this.cerca();
  }

  setData = async (d, inizio) => {
    const date =
      d.getFullYear() +
      "-" +
      ("0" + (d.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + d.getDate()).slice(-2) +
      " " +
      ("0" + d.getHours()).slice(-2) +
      ":" +
      ("0" + d.getMinutes()).slice(-2) +
      ":00";
    if (inizio) {
      await this.setState({ dataInizio: date });
    } else {
      await this.setState({ dataFine: date });
    }
    this.cerca();
  };

  setProvincia = async loc => {
    await this.setState({ provincia: loc });
    this.cerca();
    if (loc != "") {
      this.setState({ vediCheck: true });
    } else {
      this.setState({ vediCheck: false });
    }
  };

  setCategorie = async cat => {
    await this.setState({ categorie: cat });
    this.cerca();
  };

  setArtisti = async art => {
    await this.setState({ artisti: art });
    this.cerca();
  };

  setCerca = async cer => {
    await this.setState({ cerca: cer });
    this.cerca();
  };

  setToggle = async val => {
    await this.setState({ toggleRegione: val });
    this.cerca();
  };

  getFalse = () => {
    return false;
  };

  cerca = async () => {
    let eventi = [];
    await this.setState({ eventi });

    const transport = axios.create({
      withCredentials: true
    });

    transport
      .post(
        this.context.phpPath + "caricaEventi.php",
        {
          ricerca: this.state.cerca,
          categorie: this.state.categorie,
          provincia: this.state.provincia,
          regione: this.state.toggleRegione,
          artisti: this.state.artisti,
          dataInizio: this.state.dataInizio,
          dataFine: this.state.dataFine,
          pagina: this.state.pagina,
          sessionId: this.context.getSessionId()
        },
        {
          headers: { "content-type": "application/json" }
        }
      )
      .then(response => {
        if (response.data.success == true) {
          if (
            response.data.data[0] ==
            "Nessun evento corrisponde ai criteri di ricerca"
          ) {
            this.setState({
              errore: "Nessun evento corrisponde ai criteri di ricerca."
            });
          } else if (
            !(
              response.data.data[0] ==
              "Nessun evento corrisponde ai criteri di ricerca"
            )
          ) {
            this.setState({
              altriEventi: response.data.data[0][0].altri_eventi
            });
            if (response.data.data[0].length > 0) {
              for (let i = 0; i < response.data.data[0].length; i++) {
                eventi.push(
                  <EventoPreview
                    key={i}
                    id={response.data.data[0][i].id}
                    titolo={response.data.data[0][i].titolo}
                    provincia={response.data.data[0][i].provincia}
                    data={response.data.data[0][i].dataEvento}
                    descrizione={response.data.data[0][i].descrizione}
                    cerca={this.cerca}
                  />
                );
              }
            } else {
              eventi.push(
                <div className="flex text-3xl text-red-600 justify-center">
                  Nessun elemento corrisponde alla ricerca
                </div>
              );
            }
            this.setState({ eventi });
            this.setState({
              errore: ""
            });
          }
        } else {
          if (response.data.data.msg == "Qualcosa è andato storto") {
            this.setState({
              errore: "Qualcosa è andato storto."
            });
          }
        }
      });
  };

  loadEventi = () => {
    return this.state.eventi;
  };

  render() {
    return (
      <div>
        <div className=" pt-5 pb-5">
          <div className="px-8 w-full ">
            <div className="px-2 md:px-8 h-full border-b-2 general-border rounded-lg box-background w-full pt-2 pb-2 md:pt-4 md:pt-4">
              <span className=" md:flex md:items-center md:justify-right">
                <div className="relative w-full mb-3 md:pl-5 md:pr-5 mt-0">
                  <label className="block uppercase text-xs font-bold mt-2 mb-2">
                    Cerca
                  </label>
                  <input
                    type="text"
                    className="relative box-border border-0 border-solid px-3 bg-white flex w-full h-9 text-sm flex-wrap rounded items-center shadow justify-between inline-block focus:outline-none focus:shadow-outline text-black "
                    placeholder="Cerca un evento"
                    onBlur={e => {
                      this.setCerca(e.target.value);
                    }}
                  />
                </div>
                <div className="relative w-full mb-3 md:pl-5 md:pr-5">
                  <div className="block uppercase text-xs font-bold mt-2 mb-2">
                    Categorie
                  </div>
                  <Categories
                    setCategorie={this.setCategorie}
                    getCategorieError={this.getFalse}
                    getCategorieValidated={this.getFalse}
                  />
                </div>
                <div className="relative w-full mb-3 md:pl-5 md:pr-5">
                  <div className="block uppercase text-xs font-bold mt-2 mb-2">
                    Provincia
                  </div>
                  <Location
                    setProvincia={this.setProvincia}
                    getProvinceError={this.getFalse}
                    getProvinceValidated={this.getFalse}
                  />
                  <span className="flex">
                    <input
                      type="checkbox"
                      className={
                        this.state.vediCheck == true ? "flex mr-2" : "hidden"
                      }
                      onChange={e => {
                        this.setToggle(e.target.checked);
                      }}
                    />
                    <p
                      className={
                        this.state.vediCheck == true ? "flex" : "hidden"
                      }
                    >
                      Seleziona l'intera regione di appartenenza
                    </p>
                  </span>
                </div>
              </span>
              <span className=" md:flex md:items-center md:justify-right">
                <div className="relative w-full mb-3 md:pl-5 md:pr-5">
                  <div className="block uppercase text-xs font-bold mt-2 mb-2">
                    Artisti
                  </div>
                  <Artisti
                    setArtisti={this.setArtisti}
                    getArtistiError={this.getFalse}
                    getArtistiValidated={this.getFalse}
                  />
                </div>
                <div className="relative w-full mb-3 md:pl-5 md:pr-5">
                  <div className="block uppercase text-xs font-bold mt-2 mb-2">
                    Data Inizio
                  </div>
                  <Data setData={this.setData} inizio={!this.getFalse()} />
                </div>
                <div className="relative w-full mb-3 md:pl-5 md:pr-5">
                  <div className="block uppercase text-xs font-bold mt-2 mb-2">
                    Data Fine
                  </div>
                  <Data setData={this.setData} inizo={this.getFalse()} />
                </div>
              </span>
              <div
                className={
                  this.state.errore == ""
                    ? "hidden"
                    : "text-md text-red-600 flex justify-center "
                }
              >
                {this.state.errore}
              </div>
            </div>
          </div>
        </div>
        <div>{this.loadEventi()}</div>
        <div className="flex flex-row items-center justify-between p-5 border-t footer-border ">
          <button
            className={
              this.state.pagina <= 0
                ? "hidden "
                : "px-4 py-2 font-semibold rounded general-testo box-background"
            }
            onClick={async () => {
              let pag = this.state.pagina - 1;
              await this.setState({ pagina: pag });
              this.cerca();
            }}
          >
            Indietro
          </button>

          <div className={this.state.pagina <= 0 ? "" : "hidden"} />

          <button
            className={
              this.state.altriEventi
                ? "px-4 py-2 font-semibold rounded general-testo box-background"
                : "hidden"
            }
            onClick={async () => {
              let pag = this.state.pagina + 1;
              await this.setState({ pagina: pag });
              this.cerca();
            }}
          >
            Avanti
          </button>
        </div>
      </div>
    );
  }
}

function Data(props) {
  const [startDate, setStartDate] = useState(new Date());

  return (
    <DatePicker
      className="relative box-border border-0 border-solid px-3 bg-white flex w-full h-9 text-sm flex-wrap rounded items-center shadow justify-between inline-block focus:outline-none focus:shadow-outline text-black "
      selected={startDate}
      onChange={date => {
        if (date != null) {
          setStartDate(date);
          props.setData(date, props.inizio);
        } else {
          let newData = new Date();
          setStartDate(newData);
          props.setData(newData, props.inizio);
        }
      }}
      dateFormat="dd-MM-yyyy HH:mm"
      minDate={new Date()}
      showTimeInput
      closeOnScroll={true}
    />
  );
}
