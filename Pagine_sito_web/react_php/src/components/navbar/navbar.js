import React, { useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { ToggleSearch, ToggleTheme } from "../../ui/toggles";
import { CostantiContext } from "../../context/costantiContext";

export class Navbar extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <header>
        {this.context.windowWidth < 640 ? (
          <NavMobile
            theme={this.context.theme}
            user={this.context.user}
            remove={this.context.remove}
            phpPath={this.context.phpPath}
            getSessionId={this.context.getSessionId}
          />
        ) : (
          <NavDesktop
            theme={this.context.theme}
            user={this.context.user}
            remove={this.context.remove}
            phpPath={this.context.phpPath}
            getSessionId={this.context.getSessionId}
          />
        )}
      </header>
    );
  }
}

function NavMobile(props) {
  const [open, setOpen] = useState(false);
  const imgSrc = window.location.origin + "/img/Birra.png";
  const menuIcon = (
    <path
      fillRule="evenodd"
      fill={props.theme == "dark" ? "#4ecca3" : "#eeeeee"}
      d={
        open
          ? "M18.278 16.864a1 1 0 0 1-1.414 1.414l-4.829-4.828-4.828 4.828a1 1 0 0 1-1.414-1.414l4.828-4.829-4.828-4.828a1 1 0 0 1 1.414-1.414l4.829 4.828 4.828-4.828a1 1 0 1 1 1.414 1.414l-4.828 4.829 4.828 4.828z"
          : "M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"
      }
    />
  );
  const aClass =
    "mt-1 py-1 block font-semibold rounded px-2 navbar-testo hover:bg-canello-1400 md:mt-0 text-center";
  const mobileNavDiv = "px-2 pt-4 pb-4 block justify-center flex";

  return (
    <header>
      <div className="flex items-center justify-between px-4 py-3 md:p-0 border-b-2 navbar-border navbar-background">
        <div className="flex">
          <button
            onClick={() => setOpen(!open)}
            type="button"
            className="focus:outline-none"
          >
            <svg className="h-6 w-6 fill-current" viewBox="0 0 24 24">
              {menuIcon}
            </svg>
          </button>
        </div>

        <div className="mx-auto px-3 flex justify-center items-center">
          <Link to="/index">
            <img
              className="h-10 w-10 justify-center items-center"
              src={imgSrc}
              alt="logos"
            />
          </Link>
        </div>

        <div className="justify-center items-center flex">
          {/*<div>{<ToggleSearch />}</div>*/}
          <div>{<ToggleTheme />}</div>
        </div>
      </div>
      <div
        className={
          open
            ? "px-2 pt-4 pb-4 block navbar-background"
            : "px-2 pt-4 pb-4 hidden md:flex w-full items-center"
        }
      >
        <div
          className={
            open
              ? mobileNavDiv
              : "px-2 pt-4 pb-4 hidden md:flex w-full justify-center items-center"
          }
        >
          {props.user != "" ? (
            <div>
              <Link to="/visualizzaEventi" className={aClass}>
                Visualizza eventi
              </Link>
              <Link to="/inserisciEventi" className={aClass}>
                Inserisci eventi
              </Link>
            </div>
          ) : (
            <Link to="/visualizzaEventi" className={aClass}>
              Visualizza eventi
            </Link>
          )}
        </div>

        <div
          className={
            open
              ? mobileNavDiv
              : "px-2 pt-4 pb-4 hidden md:flex justify-end items-center"
          }
        >
          {props.user != "" ? (
            <div>
              <Link to="/utente" className={aClass}>
                Utente
              </Link>
              <button
                className={aClass}
                onClick={() => {
                  props.remove();
                  const transport = axios.create({
                    withCredentials: true
                  });

                  transport
                    .post(
                      props.phpPath + "logout.php",
                      {
                        sessionId: props.getSessionId()
                      },
                      {
                        headers: { "content-type": "application/json" }
                      }
                    )
                    .then(response => {});
                  window.location.replace("/");
                }}
              >
                Logout
              </button>
            </div>
          ) : (
            <div>
              <Link to="/login" className={aClass}>
                Login
              </Link>
              <Link to="/registrati" className={aClass}>
                Registrati
              </Link>
            </div>
          )}
        </div>
      </div>
    </header>
  );
}

function NavDesktop(props) {
  const aClass =
    "mt-1 py-1 block font-semibold rounded px-2 navbar-testo hover:bg-canello-1400 md:mt-0  text-center";
  const imgSrc = window.location.origin + "/img/Birra.png";
  return (
    <header className="md:flex md:justify-between md:px-4 md:py-3 md:items-center border-b-2 navbar-border navbar-background">
      <div className="flex items-center justify-between px-4 py-3 md:p-0">
        <div className=" mx-auto px-3 flex justify-center items-center">
          <Link to="/index" className="h-10 w-10 justify-center items-center">
            <img src={imgSrc} alt="logos" />
          </Link>
        </div>
      </div>
      <div className="px-2 pt-4 pb-4 hidden md:flex w-full items-center">
        <div className="px-2 pt-4 pb-4 hidden md:flex w-full justify-center items-center">
          {props.user != "" ? (
            <div className="justify-center items-center flex">
              <Link to="/visualizzaEventi" className={aClass}>
                Visualizza eventi
              </Link>
              <Link to="/inserisciEventi" className={aClass}>
                Inserisci eventi
              </Link>
            </div>
          ) : (
            <Link to="/visualizzaEventi" className={aClass}>
              Visualizza eventi
            </Link>
          )}
        </div>

        <div className="hidden md:flex justify-end items-center">
          {props.user != "" ? (
            <div className="justify-center items-center flex">
              <Link to="/utente" className={aClass}>
                Utente
              </Link>
              <button
                className={aClass}
                onClick={() => {
                  props.remove();
                  const transport = axios.create({
                    withCredentials: true
                  });

                  transport
                    .post(
                      props.phpPath + "logout.php",
                      {
                        sessionId: props.getSessionId()
                      },
                      {
                        headers: { "content-type": "application/json" }
                      }
                    )
                    .then(response => {});
                  window.location.replace("/");
                }}
              >
                Logout
              </button>
            </div>
          ) : (
            <div className="justify-center items-center flex">
              <Link to="/login" className={aClass}>
                Login
              </Link>
              <Link to="/registrati" className={aClass}>
                Registrati
              </Link>
            </div>
          )}
        </div>
      </div>
      <div className="justify-center items-center flex">
        {/*<div>{<ToggleSearch />}</div>*/}
        <div>{<ToggleTheme />}</div>
      </div>
    </header>
  );
}
