import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { CostantiContext } from "../../context/costantiContext";

export class Login extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      userError: false,
      passError: false,
      errore: ""
    };
  }

  checkUserameEmail = usernameEmail => {
    const user = /^[a-zA-Z0-9\!\_\+\-\*\#\$\^\.]+$/;
    const email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return user.test(usernameEmail) || email.test(usernameEmail);
  };

  render() {
    if (this.context.logged()) {
      window.location.replace("/");
      return <div></div>;
    }
    return (
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full md:w-4/12 px-4 pt-32 pb-32">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg box-background border-0">
              {/*<div className="rounded-t mb-0 px-6 py-6">
                <div className="text-center mb-3">
                  <h6 className="text-gray-600 text-sm font-bold">Sign in with</h6>
                </div>
                <div className="btn-wrapper text-center">
                  <button
                    className="bg-white active:bg-gray-100 text-gray-800 font-normal px-4 py-2 rounded outline-none focus:outline-none mr-2 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs"
                    type="button"
                  >
                    Github
                  </button>
                  <button
                    className="bg-white active:bg-gray-100 text-gray-800 font-normal px-4 py-2 rounded outline-none focus:outline-none mr-1 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs"
                    type="button"
                  >
                    Google
                  </button>
                </div>
                <hr className="mt-6 border-b-1 border-gray-400" />
              </div>*/}
              <div className="flex-auto px-4 md:px-10 py-10 pt-0">
                {/*
                <div className="text-gray-500 text-center mb-3 font-bold">
                  <h6 className="text-gray-600 text-sm font-bold">
                    Or sign in with credentials
                  </h6>
                </div>
                */}
                <form className="mt-6">
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Nome utente/Email
                    </label>
                    <input
                      type="username"
                      className={
                        this.state.userError
                          ? "text-black placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full bg-red-300"
                          : "text-black placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                      }
                      placeholder="Nome utente/Email"
                      autoComplete="username"
                      onChange={e => {
                        if (this.checkUserameEmail()) {
                          this.setState({ username: e.target.value });
                        }
                      }}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Password
                    </label>
                    <input
                      type="password"
                      className={
                        this.state.passError || this.state.userError
                          ? "text-black placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full bg-red-300"
                          : "text-black placeholder-gray-750 px-3 py-3 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                      }
                      placeholder="Password"
                      autoComplete="current-password"
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                    />
                  </div>
                  <div>
                    <label className="inline-flex items-center cursor-pointer">
                      <input
                        id="customCheckLogin"
                        type="checkbox"
                        className="form-checkbox text-gray-800 ml-1 w-5 h-5"
                        name="rememberMe"
                        value="true"
                        onChange={e => {
                          this.context.updateRememberMe(e.target.checked);
                        }}
                      />
                      <span className="ml-2 text-sm font-semibold">
                        Remember me
                      </span>
                    </label>
                  </div>
                  <div className="text-center mt-6">
                    <button
                      className="box-button-background active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                      type="submit"
                      name="login"
                      value="login"
                      onClick={e => {
                        e.preventDefault();
                        if (
                          this.state.user != "" &&
                          this.state.password != ""
                        ) {
                          axios
                            .post(
                              this.context.phpPath + "login.php",
                              {
                                username: this.state.username,
                                password: this.state.password,
                                sessionId: this.context.getSessionId()
                              },
                              {
                                headers: { "content-type": "application/json" }
                              }
                            )
                            .then(response => {
                              this.setState({ userError: false });
                              this.setState({ passError: false });
                              if (response.data.success == true) {
                                this.context.updateUser(
                                  response.data.data[0].user_nickname
                                );
                                this.context.updateUserPrivileges(
                                  response.data.data[0].permessoUtente
                                );
                                this.context.updateSessionId(
                                  response.data.data[0].sessionId
                                );
                              } else {
                                if (
                                  response.data.data.msg == "Password invalida!"
                                ) {
                                  this.setState({ passError: true });
                                  this.setState({
                                    errore: "Password invalida!"
                                  });
                                } else if (
                                  response.data.data.msg ==
                                  "Utente non trovato!"
                                ) {
                                  this.setState({ userError: true });
                                  this.setState({
                                    errore: "Utente non trovato!"
                                  });
                                } else if (
                                  response.data.data.msg ==
                                  "Qualcosa è andato storto"
                                ) {
                                  this.setState({
                                    errore: "Qualcosa è andato storto"
                                  });
                                }
                              }
                            });
                        }
                      }}
                    >
                      Sign In
                    </button>
                  </div>
                  <div
                    className={
                      this.state.errore == ""
                        ? "hidden"
                        : "text-md text-red-600 flex justify-center "
                    }
                  >
                    {this.state.errore}
                  </div>
                </form>
              </div>
            </div>
            <div className="flex flex-wrap mt-6">
              <div className="w-full text-center">
                <Link to="/registrati" className="hover:underline">
                  <small>Create new account</small>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

//export default Login;
