import React from "react";
import axios from "axios";
import ReactStars from "react-rating-stars-component";
import { useLocation } from "react-router-dom";
import { CostantiContext } from "../../context/costantiContext";
import { Commento } from "../commento/commento";
import { Link } from "react-router-dom";

export class Evento extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);

    this.state = {
      titolo: "",
      provincia: "",
      data: "",
      artisti: [],
      descrizione: "",
      id: this.getId(),
      commenti: [],
      errore: "",
      nicknameMembro: "",
      commento: "",
      voto: "",
      pagina: 0,
      altriCommenti: true,
      voto_medio: 0,
      categorie: []
    };
  }

  getId = () => {
    let path = window.location.pathname;
    let ids = path.split("/");
    return parseInt(ids[2], 10);
  };

  componentDidMount() {
    this.carica();
  }

  carica = async () => {
    let commenti = [];

    const transport = axios.create({
      withCredentials: true
    });

    transport
      .post(
        this.context.phpPath + "caricaEventoSingolo.php",
        {
          id: this.state.id,
          sessionId: this.context.getSessionId()
        },
        {
          headers: { "content-type": "application/json" }
        }
      )
      .then(response => {
        if (response.data.success == true) {
          let voto_medio = response.data.data[0].voto_medio;
          voto_medio = voto_medio.toString();
          voto_medio = voto_medio.substring(0, 1);
          this.setState({ artisti: response.data.data[0].artisti });
          this.setState({ voto_medio });
          this.setState({ categorie: response.data.data[0].categorie });
          this.setState({ data: response.data.data[0].data });
          this.setState({ descrizione: response.data.data[0].descrizione });
          this.setState({
            nicknameMembro: response.data.data[0].nicknameMembro
          });
          this.setState({ provincia: response.data.data[0].provincia });
          this.setState({ titolo: response.data.data[0].titolo });
          this.setState({
            altriCommenti: response.data.data[0].altri_commenti
          });

          if (response.data.data[0].commenti.length > 0) {
            for (let i = 0; i < response.data.data[0].commenti.length; i++) {
              commenti.push(
                <Commento
                  key={i + 1}
                  autore={response.data.data[0].commenti[i].nicknameUtente}
                  data={response.data.data[0].commenti[i].dataCommento}
                  voto={response.data.data[0].commenti[i].voto}
                  commento={response.data.data[0].commenti[i].commento}
                  id={response.data.data[0].commenti[i].ID}
                  ricarica={this.carica}
                />
              );
            }
          } else {
            commenti.push(
              <div className="flex text-3xl text-red-600 justify-center">
                Non esistono commenti
              </div>
            );
          }
          this.setState({ commenti });
        } else {
          if (response.data.data.msg == "Qualcosa è andato storto") {
            this.setState({
              errore: "Qualcosa è andato storto."
            });
          } else if (response.data.data.msg == "Evento non trovato") {
            this.setState({
              errore: "Evento non trovato."
            });
          }
        }
      });
  };

  caricaCommenti = async () => {
    let commenti = [];

    const transport = axios.create({
      withCredentials: true
    });

    transport
      .post(
        this.context.phpPath + "caricaCommenti.php",
        {
          id: this.state.id,
          pagina: this.state.pagina,
          sessionId: this.context.getSessionId()
        },
        {
          headers: { "content-type": "application/json" }
        }
      )
      .then(response => {
        if (response.data.success == true) {
          this.setState({
            altriCommenti: response.data.data[0].altri_commenti
          });
          if (response.data.data[0].commenti.length > 0) {
            for (let i = 0; i < response.data.data[0].commenti.length; i++) {
              commenti.push(
                <Commento
                  key={i + 1}
                  autore={response.data.data[0].commenti[i].nicknameUtente}
                  data={response.data.data[0].commenti[i].dataCommento}
                  voto={response.data.data[0].commenti[i].voto}
                  commento={response.data.data[0].commenti[i].commento}
                  id={response.data.data[0].commenti[i].ID}
                  ricarica={this.carica}
                />
              );
            }
          } else {
            commenti.push(
              <div className="flex text-3xl text-red-600 justify-center">
                Non esistono commenti
              </div>
            );
          }
          this.setState({ commenti });
        } else {
          if (response.data.data.msg == "Qualcosa è andato storto") {
            this.setState({
              errore: "Qualcosa è andato storto."
            });
          } else if (response.data.data.msg == "Evento non trovato") {
            this.setState({
              errore: "Evento non trovato."
            });
          }
        }
      });
  };

  getVotoMedio = () => {
    return this.state.voto_medio;
  };

  render() {
    let elimina =
      this.context.userPrivileges == "A" ||
      this.context.userPrivileges == "a" ? (
        <div className="md:pl-5 md:flex-col">
          <div className="justify-center mt-8 md:mt-auto flex md:mx-8 md:pt-2 md:pb-2 md:flex-shrink-0">
            <div className="md:flex-col  rounded-md shadow">
              <button
                onClick={() => {
                  const transport = axios.create({
                    withCredentials: true
                  });

                  transport
                    .post(
                      this.context.phpPath + "eliminaEvento.php",
                      {
                        id: this.state.id,
                        sessionId: this.context.getSessionId()
                      },
                      {
                        headers: { "content-type": "application/json" }
                      }
                    )
                    .then(response => {
                      if (response.data.success == true) {
                        window.location.replace("/visualizzaEventi/");
                      } else {
                        if (response.data.data.msg == "Utente non loggato") {
                          this.context.remove();
                          window.location.replace("/login/");
                        } else if (
                          response.data.data.msg == "Permesso negato"
                        ) {
                          this.context.remove();
                          window.location.replace("/login/");
                        }
                      }
                    });
                }}
                className="flex-col items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md general-button-testo box-button-background"
              >
                Elimina
              </button>
            </div>
          </div>
        </div>
      ) : (
        <div></div>
      );

    let invia = (
      <div className="md:flex-col">
        <div className="justify-center mt-8 flex md:pt-2 md:pb-2 md:flex-shrink-0">
          <div className="md:flex-col  rounded-md shadow">
            <button
              onClick={() => {
                const transport = axios.create({
                  withCredentials: true
                });

                transport
                  .post(
                    this.context.phpPath + "inserisciCommento.php",
                    {
                      commento: this.state.commento,
                      voto: this.state.voto,
                      idEvento: this.state.id,
                      sessionId: this.context.getSessionId()
                    },
                    {
                      headers: { "content-type": "application/json" }
                    }
                  )
                  .then(response => {
                    if (response.data.success == true) {
                      this.carica();
                    } else {
                      if (response.data.data.msg == "Utente non loggato") {
                        this.context.remove();
                        window.location.replace("/login/");
                      } else if (
                        response.data.data.msg == "Qualcosa è andato storto" ||
                        response.data.data.msg == "Evento non trovato"
                      ) {
                        this.setState({
                          errore: "Qualcosa è andato storto."
                        });
                      }
                    }
                  });
              }}
              className="flex-col items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md general-button-testo box-button-background"
            >
              Invia
            </button>
          </div>
        </div>
      </div>
    );
    let artisti = [];
    let artistiOrigin = this.state.artisti;

    artistiOrigin.forEach((item, i) => {
      artisti.push(<Artisti key={999 - i} artista={item} />);
    });

    let categorie = [];
    let categorieOrigin = this.state.categorie;

    categorieOrigin.forEach((item, i) => {
      categorie.push(<Categorie key={9999 - i} categoria={item} />);
    });

    const ratingChanged = async newRating => {
      await this.setState({ voto: newRating });
    };
    const setCommenti = async val => {
      await this.setState({ commento: val });
    };

    const commenti = this.context.logged() ? (
      <div className="flex-col border-t-2 mx-auto mt-2 py-4 px-2 md:py-4 md:px-4 md:flex md:items-center md:justify-between md:items-center">
        <span className=" md:flex md:items-center md:justify-right">
          <div className="relative w-full mb-3 md:pl-5 md:pr-5">
            <label className="block uppercase text-xs font-bold mb-2">
              commento
            </label>
            <textarea
              type="commento"
              className="px-3 text-black py-3 placeholder-gray-750 bg-white rounded text-md shadow focus:outline-none focus:shadow-outline w-full"
              placeholder="commento"
              maxLength="102400"
              cols="60"
              onBlur={e => {
                setCommenti(e.target.value);
              }}
            />
          </div>
          <div className="flex flex-col md:ml-auto md:mr relative w-full  mb-3 md:pl-5 md:pr-5">
            Voto:
            <ReactStars
              key="0"
              count={5}
              onChange={ratingChanged}
              size={24}
              activeColor="#ffd700"
            />
          </div>
          {invia}
        </span>
      </div>
    ) : (
      <div></div>
    );

    return (
      <div>
        <div className="pt-5 pl-8 underline text-left ">
          <Link to="/visualizzaEventi" className="">
            &#129044; Torna a visualizza eventi
          </Link>
        </div>
        <div className="pt-5 pb-5">
          <div className="px-8 w-full md:flex ">
            <div className="flex flex-col px-2 md:flex md:px-8 h-full  rounded-lg box-background w-full pt-2 pb-2 md:pb-4 md:pt-4">
              <div className="mx-auto w-full py-4 px-2 md:py-4 md:px-4 md:flex md:items-center md:justify-between  navbar-border">
                <div className="md:float-left md:w-full tracking-tight text-center md:px-8 md:text-left general-testo md:text-4xl">
                  <span className="md:flex md:items-center md:justify-right">
                    <p className="text-3xl  break-words font-extrabold">
                      {this.state.titolo}
                    </p>
                    <div className="md:flex md:ml-auto ">
                      <span className="block  break-words text-xl md:mr-5">
                        {this.state.provincia}
                      </span>

                      <span className="block  break-words text-xl">
                        {this.state.data}
                      </span>
                    </div>
                  </span>
                  <span className="text-xl font-bold break-words general-testo">
                    {this.state.descrizione}
                  </span>
                </div>
                <div className="flex-col max-w-52 mx-auto gap-2 items-center justify-center  text-base font-medium  general-testo  ">
                  <Stars voto={this.getVotoMedio()} />
                </div>

                {elimina}
              </div>
              <p className="text-center break-words mt-4 mb-4">Artisti</p>
              <span className=" mx-auto w-full h-full md:inline-flex  break-words content-between flex-wrap md:items-center md:justify-between">
                <div className="md:inline-flex gap-2  break-words flex-wrap content-between md:justify-center  ml-auto mr-auto">
                  {artisti}
                </div>
              </span>
              <p className="text-center break-words mt-4 mb-4">Categorie</p>
              <span className=" mx-auto w-full h-full md:inline-flex  break-words content-between flex-wrap md:items-center md:justify-between">
                <div className="md:inline-flex gap-2  break-words flex-wrap content-between md:justify-center  ml-auto mr-auto">
                  {categorie}
                </div>
              </span>

              {commenti}
            </div>
          </div>
        </div>
        {this.state.errore != "" ? (
          <div className="text-md text-red-600 flex justify-center ">
            {this.state.errore}
          </div>
        ) : (
          this.state.commenti
        )}
        <div className="flex flex-row items-center justify-between p-5 border-t footer-border ">
          <button
            className={
              this.state.pagina <= 0
                ? "hidden "
                : "px-4 py-2 font-semibold rounded general-testo box-background"
            }
            onClick={async () => {
              let pag = this.state.pagina - 1;
              await this.setState({ pagina: pag });
              this.caricaCommenti();
            }}
          >
            Indietro
          </button>

          <div className={this.state.pagina <= 0 ? "" : "hidden"} />

          <button
            className={
              this.state.altriCommenti
                ? "px-4 py-2 font-semibold rounded general-testo box-background"
                : "hidden"
            }
            onClick={async () => {
              let pag = this.state.pagina + 1;
              await this.setState({ pagina: pag });
              this.caricaCommenti();
            }}
          >
            Avanti
          </button>
        </div>
      </div>
    );
  }
}

function Artisti(props) {
  return (
    <div className="flex-col flex  break-words my-2 w-min-52 mx-auto md:mx-5 my-5 md:my-0  items-center justify-center px-3 py-1 border border-transparent text-base font-medium rounded-md general-button-testo box-button-background ">
      <p className="justify-center  text-center w-auto">{props.artista}</p>
    </div>
  );
}

function Stars(props) {
  let star = (
    <svg
      className="block h-8 w-8"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
    >
      <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
    </svg>
  );

  let finalStar = [];
  for (let i = 0; i < props.voto; i++) {
    finalStar.push(star);
  }
  return <div className="flex justify-center">{finalStar}</div>;
}

function Categorie(props) {
  return (
    <div className="flex-col flex my-2 w-min-52 mx-auto md:mx-5 my-5 md:my-0  items-center justify-between px-3 py-1 border border-transparent text-base font-medium rounded-md general-button-testo box-button-background ">
      <p className="justify-center text-center w-auto">{props.categoria}</p>
    </div>
  );
}
