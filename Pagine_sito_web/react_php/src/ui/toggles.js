import React, { useState } from "react";
import { CostantiContext } from "../context/costantiContext";

export class ToggleSearch extends React.Component {
  static contextType = CostantiContext;

  constructor(props) {
    super(props);
  }

  render() {
    return <ToggleSearchFunction theme={this.context.theme} />;
  }
}

function ToggleSearchFunction(props) {
  const [open, setOpen] = useState(false);

  return (
    <div>
      <div className="transition duration-500 ease-in-out rounded-full flex">
        <div className={!open ? "hidden" : "rounded-xl flex"}>
          <input
            type="search"
            className="text-black pl-3 black rounded-xl cursor-text flex outline-none"
            placeholder="Cerca evento"
          />
        </div>

        <div className="pl-2 flex"></div>
        <button
          onClick={() => setOpen(!open)}
          className="cursor-pointer text-yellow-500 justify-center items-center"
        >
          <svg
            width="24"
            className="w-6 h-6"
            height="24"
            xmlns="http://www.w3.org/2000/svg"
            fillRule="evenodd"
            clipRule="evenodd"
          >
            <path
              fill={props.theme === "navbar-toggle"}
              d="M15.853 16.56c-1.683 1.517-3.911 2.44-6.353 2.44-5.243 0-9.5-4.257-9.5-9.5s4.257-9.5 9.5-9.5 9.5 4.257 9.5 9.5c0 2.442-.923 4.67-2.44 6.353l7.44 7.44-.707.707-7.44-7.44zm-6.353-15.56c4.691 0 8.5 3.809 8.5 8.5s-3.809 8.5-8.5 8.5-8.5-3.809-8.5-8.5 3.809-8.5 8.5-8.5z"
            />
          </svg>
        </button>
      </div>
    </div>
  );
}

export class ToggleTheme extends React.Component {
  static contextType = CostantiContext;
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ToggleThemeFunction
        theme={this.context.theme}
        setTheme={this.context.updateTheme}
      />
    );
  }
}

function ToggleThemeFunction(props) {
  return (
    <div className="transition duration-500 ease-in-out rounded-full p-2 flex">
      <button
        onClick={() => props.setTheme()}
        className="cursor-pointer navbar-toggle justify-center items-center"
      >
        <svg
          className="w-6 h-6"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M10 2a1 1 0 011 1v1a1 1 0 11-2 0V3a1 1 0 011-1zm4 8a4 4 0 11-8 0 4 4 0 018 0zm-.464 4.95l.707.707a1 1 0 001.414-1.414l-.707-.707a1 1 0 00-1.414 1.414zm2.12-10.607a1 1 0 010 1.414l-.706.707a1 1 0 11-1.414-1.414l.707-.707a1 1 0 011.414 0zM17 11a1 1 0 100-2h-1a1 1 0 100 2h1zm-7 4a1 1 0 011 1v1a1 1 0 11-2 0v-1a1 1 0 011-1zM5.05 6.464A1 1 0 106.465 5.05l-.708-.707a1 1 0 00-1.414 1.414l.707.707zm1.414 8.486l-.707.707a1 1 0 01-1.414-1.414l.707-.707a1 1 0 011.414 1.414zM4 11a1 1 0 100-2H3a1 1 0 000 2h1z"
            clipRule="evenodd"
          ></path>
        </svg>
      </button>
    </div>
  );
}
