import React from "react";
import { Utente } from "../components/utente/utente";
import { Navbar } from "../components/navbar/navbar";
import { Footer } from "../components/footer/footer";
import { CookieConsentBanner } from "../components/cookieConsent/cookieConsent";

const UtentePage = () => {
  return (
    <div className="Utente flex flex-col min-h-screen">
      <Navbar />
      <main className="flex-grow">
        <Utente />
      </main>
      <Footer />
      <CookieConsentBanner />
    </div>
  );
};

export default UtentePage;
