import React from "react";
import { Evento } from "../components/evento/evento";
import { Navbar } from "../components/navbar/navbar";
import { Footer } from "../components/footer/footer";
import { CookieConsentBanner } from "../components/cookieConsent/cookieConsent";

const EventoPage = () => {
  return (
    <div className="eventi flex flex-col min-h-screen">
      <Navbar />
      <main className="flex-grow">
        <Evento />
      </main>
      <Footer />
      <CookieConsentBanner />
    </div>
  );
};

export default EventoPage;
