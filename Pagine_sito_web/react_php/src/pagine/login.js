import React from "react";
import { Login } from "../components/login/login";
import { Navbar } from "../components/navbar/navbar";
import { Footer } from "../components/footer/footer";
import { CookieConsentBanner } from "../components/cookieConsent/cookieConsent";

const LoginPage = () => {
  return (
    <div className="Login flex flex-col min-h-screen">
      <Navbar />
      <main className="flex-grow">
        <Login />
      </main>
      <Footer />
      <CookieConsentBanner />
    </div>
  );
};

export default LoginPage;
