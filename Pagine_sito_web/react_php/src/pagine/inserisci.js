import React from "react";
import { InserisciEvento } from "../components/inseriscievento/inseriscievento";
import { Navbar } from "../components/navbar/navbar";
import { Footer } from "../components/footer/footer";
import { CookieConsentBanner } from "../components/cookieConsent/cookieConsent";

const InserisciEventoPage = () => {
  return (
    <div className="InserisciEvento flex flex-col min-h-screen">
      <Navbar />
      <main className="flex-grow">
        <InserisciEvento />
      </main>
      <Footer />
      <CookieConsentBanner />
    </div>
  );
};

export default InserisciEventoPage;
