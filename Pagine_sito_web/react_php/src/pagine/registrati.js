import React from "react";
import { Register } from "../components/registrati/registrati";
import { Navbar } from "../components/navbar/navbar";
import { Footer } from "../components/footer/footer";
import { CookieConsentBanner } from "../components/cookieConsent/cookieConsent";

const RegisterPage = () => {
  return (
    <div className="Register flex flex-col min-h-screen">
      <Navbar />
      <main className="flex-grow">
        <Register />
      </main>
      <Footer />
      <CookieConsentBanner />
    </div>
  );
};

export default RegisterPage;
