import React from "react";
import { Navbar } from "../components/navbar/navbar";
import { Footer } from "../components/footer/footer";
import { CookieConsentBanner } from "../components/cookieConsent/cookieConsent";

const IndexPage = () => {
  return (
    <div className="Index flex flex-col min-h-screen">
      <Navbar />
      <main className="flex flex-grow justify-center mx-8">
        <div className="flex index-testo  flex-col">
          <div className="w-full items-enter justify-center ">
            <img
              className="h-full m-auto pt-20 justify-center items-center "
              src="./img/Birra.png"
              alt="logos"
            />
          </div>
          <h1 className="justify-center text-5xl pt-6 pb-6 text-center uppercase">
            Il progetto:
          </h1>
          <div className="text-2xl">
            <p className="w-full pb-2 text-center">
              Questo lavoro è ipoteticamente commissionato da un'azienda di
              servizi culturali e ricreativi. <br />
              Il progetto &egrave; incentrato nella realizzazione di un sistema
              informatico per la gestione online di una web community nella
              quale è possibile commentare e valutare determinati eventi dal
              vivo. <br />
              Oltre a questa funzionailt&agrave; &egrave; possibile aggiungere,
              da parte degli utenti, nuovi eventi con relativa categoria e
              partecipanti. <br />
            </p>
            <h1 className="justify-center text-5xl pt-6 pb-6 text-center uppercase">
              I 5:
            </h1>
            <p className="w-full pb-2 text-center">
              Gruppo nato dal caso da dei compagni di banco con un ottima
              collaborazione e intesa fin dall'inizio. <br />
              Baldo Lorenzo,responsabile del progetto, ha organizzato il gruppo
              e si è specializzato nella gestione della documentazione. <br />
              Canello Leonardo, sviluppatore, si è specializzato nella sicurezza
              e nella gestione della palette colori. <br />
              Lucietto Denis, sviluppatore, ha posto il progetto davanti alla
              necessità di dormire e si è specializzato nel JavaScript e PHP.{" "}
              <br />
              Rizzolo Filippo, sviluppatore, si è specializzato nella
              strutturazione del sito. <br />
              Trevisan Gabriele, sviluppatore, si è specializzato nell´SQL e
              PHP.
            </p>
            <h1 className="justify-center text-5xl pt-8 pb-6 text-center uppercase">
              Le tecnologie usate:
            </h1>
            <span className="md:flex">
              <div className="md:w-1/4 md:mr-20">
                <img
                  className="h-20  mx-auto md:ml-0 "
                  src="./img/React.png"
                  alt="logos"
                />
              </div>
              <p className="w-full pt-5 pb-2">
                React, libreria open-surce e front-end, utilizza JavaScript per
                la creazione di interfacce utente.
              </p>
            </span>
            <span className="md:flex">
              <div className="md:w-1/4 md:mr-20">
                <img
                  className="h-20  mx-auto sm:ml-0 "
                  src="./img/TailwindCss.png"
                  alt="logos"
                />
              </div>
              <p className="w-full pt-3 pb-2">
                Tailwindcss, framework css, punta a rivoluzionare l'approccio
                alla stilizzazione grafica implementando una serie di classi
                componibili integrabili nell'html
              </p>
            </span>
            <span className="md:flex">
              <div className="md:w-1/4 md:mr-20">
                <img
                  className="h-20  mx-auto md:ml-0 "
                  src="./img/Php.png"
                  alt="logos"
                />
              </div>
              <p className="w-full pt-4 pb-2">
                Php, linguaggio di scripting interpretato, viene usato per la
                programmazione di pagine web dinamiche.
              </p>
            </span>
            <span className="md:flex">
              <div className="md:w-1/4 md:mr-20">
                <img
                  className="h-20  mx-auto md:ml-0 "
                  src="./img/MySql.png"
                  alt="logos"
                />
              </div>
              <p className="w-full pt-9">
                MySQL è un sistema open source di gestione di database
                relazionali SQL
              </p>
            </span>
            <div className="w-full h-20" />
          </div>
        </div>
      </main>
      <Footer />
      <CookieConsentBanner />
    </div>
  );
};

export default IndexPage;
