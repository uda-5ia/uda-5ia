import React from "react";
import { CaricaEventi } from "../components/caricaeventi/caricaeventi";
import { Navbar } from "../components/navbar/navbar";
import { Footer } from "../components/footer/footer";
import { CookieConsentBanner } from "../components/cookieConsent/cookieConsent";

const CaricaEventiPage = () => {
  return (
    <div className="CaricaEventi flex flex-col min-h-screen">
      <Navbar />
      <main className="flex-grow">
        <CaricaEventi />
      </main>
      <Footer />
      <CookieConsentBanner />
    </div>
  );
};

export default CaricaEventiPage;
