import React from "react";
import { PageNotFound } from "../components/404/404";

const PageNotFoundPage = () => {
  return (
    <div className="404 flex flex-col min-h-screen">
      <main className="flex-grow">
        <PageNotFound />
      </main>
    </div>
  );
};

export default PageNotFoundPage;
