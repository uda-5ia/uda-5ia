import React, { createContext, Component } from "react";

export const CostantiContext = createContext();
export class CostantiContextProvider extends Component {
  state = {
    phpPath: "https://api.cagnocammello.com/",
    user: "",
    userPrivileges: "",
    sessionId: "",
    rememberMe: false,
    windowWidth: 0,
    windowHeight: 0,
    theme: "dark"
  };

  updateUser = utente => {
    this.setState({ user: utente });
    if (this.state.rememberMe) {
      localStorage.setItem("user", JSON.stringify(utente));
    } else {
      sessionStorage.setItem("user", JSON.stringify(utente));
    }
  };

  updateUserPrivileges = permesso => {
    this.setState({ userPrivileges: permesso });
    if (this.state.rememberMe) {
      localStorage.setItem("userPrivileges", JSON.stringify(permesso));
    } else {
      sessionStorage.setItem("userPrivileges", JSON.stringify(permesso));
    }
  };

  updateRememberMe = val => {
    if (this.state.user == "" && this.state.userPrivileges == "") {
      this.setState({ rememberMe: val });
      localStorage.setItem("remember", val);
    }
  };

  updateSessionId = sess => {
    if (sess != null && sess != false) {
      this.setState({ sessionId: sess });
      localStorage.setItem("sessionId", JSON.stringify(sess));
    }
  };

  getSessionId = () => {
    if (this.state.sessionId != null && this.state.sessionId != "") {
      return this.state.sessionId;
    }
    let sess = JSON.parse(localStorage.getItem("sessionId"));
    this.setState({ sessionId: sess });
    return sess;
  };

  updateTheme = () => {
    let theme = this.state.theme;
    theme = theme == "dark" ? "light" : "dark";
    this.setState({ theme });
    localStorage.setItem("color-theme", JSON.stringify(theme));
  };

  logged = () => {
    let user = this.state.user,
      userPrivileges = this.state.userPrivileges;

    if (
      user == "undefined" ||
      userPrivileges == "undefined" ||
      user == "" ||
      userPrivileges == "" ||
      user == null ||
      userPrivileges == null
    ) {
      const r = this.restoreUser();
      user = r[0];
      userPrivileges = r[1];
    }
    if (
      user != "" &&
      userPrivileges != "" &&
      user != null &&
      userPrivileges != null &&
      user != "undefined" &&
      userPrivileges != "undefined"
    ) {
      return true;
    }

    return false;
  };

  loggedAsync = async () => {
    let user = this.state.user,
      userPrivileges = this.state.userPrivileges;

    if (
      user == "undefined" ||
      userPrivileges == "undefined" ||
      user == "" ||
      userPrivileges == "" ||
      user == null ||
      userPrivileges == null
    ) {
      const r = this.restoreUser();
      user = r[0];
      userPrivileges = r[1];
    }
    if (
      user != "" &&
      userPrivileges != "" &&
      user != null &&
      userPrivileges != null &&
      user != "undefined" &&
      userPrivileges != "undefined"
    ) {
      return true;
    }

    return false;
  };

  restoreUser = () => {
    let rememberMe = JSON.parse(localStorage.getItem("remember"));
    let user;
    let userPrivileges;
    if (rememberMe) {
      sessionStorage.removeItem("user");
      sessionStorage.removeItem("userPrivileges");

      user = JSON.parse(localStorage.getItem("user"));
      userPrivileges = JSON.parse(localStorage.getItem("userPrivileges"));

      if (user != null && userPrivileges != null) {
        this.setState({ user });
        this.setState({ userPrivileges });
      }
    } else {
      localStorage.removeItem("user");
      localStorage.removeItem("userPrivileges");

      user = JSON.parse(sessionStorage.getItem("user"));
      userPrivileges = JSON.parse(sessionStorage.getItem("userPrivileges"));

      if (user != null && userPrivileges != null) {
        this.setState({ user });
        this.setState({ userPrivileges });
      }
    }

    return [user, userPrivileges];
  };

  remove = () => {
    localStorage.clear();
    sessionStorage.clear();
  };

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));

    let rememberMe = JSON.parse(localStorage.getItem("remember"));

    if (rememberMe != null) {
      this.setState({ rememberMe });
    } else {
      localStorage.setItem("remember", JSON.stringify(this.state.rememberMe));
    }

    const theme = JSON.parse(localStorage.getItem("color-theme"));

    if (theme == null) {
      localStorage.setItem("color-theme", JSON.stringify("dark"));
    }
    if (theme == "dark" || theme == "light") {
      this.setState({ theme });
    }

    this.restoreUser();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  updateDimensions() {
    let wW = typeof window !== "undefined" ? window.innerWidth : 0;
    let wH = typeof window !== "undefined" ? window.innerHeight : 0;
    this.setState({ windowWidth: wW });
    this.setState({ windowHeight: wH });
  }

  render() {
    return (
      <CostantiContext.Provider
        value={{
          ...this.state,
          updateUser: this.updateUser,
          updateUserPrivileges: this.updateUserPrivileges,
          updateRememberMe: this.updateRememberMe,
          updateSessionId: this.updateSessionId,
          updateTheme: this.updateTheme,
          logged: this.logged,
          loggedAsync: this.loggedAsync,
          getSessionId: this.getSessionId,
          remove: this.remove
        }}
      >
        {this.props.children}
      </CostantiContext.Provider>
    );
  }
}
